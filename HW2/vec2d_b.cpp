/* Lu Zhang
   SMU Mathematics
   Math 4370/6370
   19 February 2017 */

// Inclusions
#include "vec2d_b.hpp"


// This file implements the operations defined in the vec2d_b class


///// General class routines /////

// constructor (initializes values to 0.0)
vec2d_b::vec2d_b(long int len_m, long int len_n) {
  // if len_m or len_n is illegal, create an empty vector
  if (len_m < 1 || len_n < 1){
    length_m = 0;
    length_n = 0;
    data = NULL;
  } else {
    length_m = len_m;
    length_n = len_n;
    data = new double[length_m*length_n];
    for(long int i=0;i<length_m*length_n;i++){
	data[i] = 0.0;
    }
  }
}
				
			
// destructor (frees space associated with a vec2d_b)
vec2d_b::~vec2d_b() {
  if (data!=NULL)
    delete[] data;
  length_m = 0;
  length_n = 0;
}


// write myself to stdout
int vec2d_b::Write() const {
  // return with failure if data array isn't allocated
  if (data == NULL) {
    fprintf(stderr, "vec2d_b:Write error, empty data array\n");
    return 1;
  }

  // print data to screen 
  for (long int i=0; i<length_m; i++) {
    for (long int j=0; j<length_n; j++) {
      printf("  %.17g",data[i*length_n+j]);
    }
    printf("\n");
  }
  // return with success
  return 0;
}


// write myself to a file
int vec2d_b::Write(const char *outfile) const {
  // return with failure if data array isn't allocated
  if (data == NULL) {
    fprintf(stderr, "vec2d_b:Write error, empty data array\n");
    return 1;
  }

  // return with failure if 'outfile' is empty
  if (strlen(outfile) < 1) {
    fprintf(stderr, "vec2d_b::Write error, empty outfile\n");
    return 1;
  }

  // open output file
  FILE *fptr = NULL;
  fptr = fopen(outfile, "w");
  if (fptr == NULL) {
    fprintf(stderr, "vec2d_b::Write error, unable to open %s for writing\n",outfile);
    return 1;
  }

  // print data to file
  for (long int i=0; i<length_m; i++){
    for (long int j=0; j<length_n; j++){
      fprintf(fptr, "  %.17g\n",data[i*length_n+j]);
    }
    fprintf(fptr, "\n");
  }

  // close output file and return
  fclose(fptr);
  return 0;
}



///// Arithmetic operations defined on a given vec2d_b /////

// x = a*y + b*z
int vec2d_b::LinearSum(double a, const vec2d_b& y, double b, const vec2d_b& z) {
  // check that array sizes match
  if (y.length_m != length_m  ||  z.length_m != length_m || y.length_n != length_n || z.length_n != length_n) {
    fprintf(stderr,"vec2d_b::LinearSum error, vector sizes do not match\n");
    return 1;
  }
  
  // check that data is not NULL
  if (data == NULL || y.data == NULL || z.data == NULL) {
    fprintf(stderr, "vec2d_b::LinearSum error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length_m; i++){
    for(long int j=0; j<length_n; j++){
      data[i*length_n+j] = a*y.data[i*length_n+j] + b*z.data[i*length_n+j];
    }
  }
  return 0;
}


//   x = x*a  (scales my data by scalar a)
int vec2d_b::Scale(double a) {
  // check that data is not NULL
  if (data == NULL) {
    fprintf(stderr, "vec2d_b::Scale error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length_m; i++){
    for (long int j=0; j<length_n; j++){
      data[i*length_n+j] *= a;
    }
  }
  return 0;
}


//   x = y  (copies y into x)
int vec2d_b::Copy(const vec2d_b& y) {
  // check that array sizes match
  if (y.length_m != length_m || y.length_n != length_n) {
    fprintf(stderr,"vec2d_b::Copy error, vector sizes do not match\n");
    return 1;
  }
  
  // check that data is not NULL
  if (data == NULL || y.data == NULL) {
    fprintf(stderr, "vec2d_b::Copy error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length_m; i++){
    for (long int j=0; j<length_n; j++){
      data[i*length_n+j] = y.data[i*length_n+j];
    }
  }
  return 0;
}


//   x = a  (sets all entries of x to the scalar a)
int vec2d_b::Constant(double a) {
  // check that data is not NULL
  if (data == NULL) {
    fprintf(stderr, "vec2d_b::Constant error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length_m; i++){
    for (long int j=0; j<length_n; j++){
      data[i*length_n+j] = a;
    }
  }
  return 0;
}


///// scalar quantities derived from vectors /////

// min x_i
double vec2d_b::Min() {
  // check that my data is allocated
  if (data == NULL) {
    fprintf(stderr,"vec2d_b::Min error, data not allocated\n");
    return -1.0;
  }
  
  // perform operation and return
  double mn=data[0];
  for (long int i=0; i<length_m; i++){
    for (long int j=0; j<length_n; j++){
      mn = std::min(mn,data[i*length_n+j]);
    }
  }
  return mn;
}


// max x_i
double vec2d_b::Max() {
  // check that my data is allocated
  if (data == NULL) {
    fprintf(stderr,"vec2d_b::Max error, data not allocated\n");
    return -1.0;
  }
  
  // perform operation and return
  double mx=data[0];
  for (long int i=0; i<length_m; i++){
    for (long int j=0; j<length_n; j++){
      mx = std::max(mx,data[i*length_n+j]);
    }
  }
  return mx;
}



///// independent constructor routines /////

// create a vector of linearly spaced data
vec2d_b Linspace(double a, double b, long int m, long int n) {
  vec2d_b *x = new vec2d_b(m,n);
  double *xd = x->GetData();
  double h = (b-a)/(n-1);
  for (long int i=0; i<m; i++){
    for (long int j=0; j<n; j++){
      xd[i*n+j] = a + h*j;
    }
  }
  return *x;
}


// create a vector of uniformly-distributed random data
vec2d_b Random(long int m, long int n) {
  vec2d_b *x = new vec2d_b(m,n);
  double *xd = x->GetData();
  for (long int i=0; i<m; i++){
    for(long int j=0; j<n; j++){
      xd[i*n+j] = random() / (pow(2.0,31.0) - 1.0);
    }
  }
  return *x;
}


///// independent arithmetic routines /////

// dot-product of x and y
double Dot(const vec2d_b& x, const vec2d_b& y) {
  // check that array sizes match
  if (y.Length_m() != x.Length_m() || y.Length_n() != x.Length_n()) {
    fprintf(stderr,"Dot error, vector sizes do not match\n");
    return 0.0;
  }
  
  // perform operation and return
  double sum=0.0;
  for (long int i=0; i<x.Length_m(); i++){
    for(long int j=0; j<x.Length_n(); j++){
      sum += x[i*x.Length_n()+j]*y[i*x.Length_n()+j];
    }
  }
  return sum;
}



// ||x||_2
double TwoNorm(const vec2d_b& x) {
  double sum=0.0;
  for (long int i=0; i<x.Length_m(); i++){
    for(long int j=0; j<x.Length_n(); j++){
      sum += x[i*x.Length_n()+j]*x[i*x.Length_n()+j];
    }
  }
  return sqrt(sum);
}


// ||x||_RMS
double RmsNorm(const vec2d_b& x) {
  double sum=0.0;
  for (long int i=0; i<x.Length_m(); i++){
    for (long int j=0; j<x.Length_n(); j++){
      sum += x[i*x.Length_n()+j]*x[i*x.Length_n()+j];
    }
  }
  return sqrt(sum/(x.Length_m()*x.Length_n()));
}


// ||x||_infty
double MaxNorm(const vec2d_b& x) {
  double mx=0.0;
  for (long int i=0; i<x.Length_m(); i++){
    double sum=0.0;
    for(long int j=0; j<x.Length_n(); j++){
      sum = sum + std::abs(x[i*x.Length_n()+j]);
    }
    mx = std::max(mx, sum);
  }
  return mx;
}
