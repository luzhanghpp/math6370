/* Lu Zhang
   SMU Mathematics
   Math 4370/6370
   16 February 2017 */

// Inclusions
#include <stdlib.h>
#include <stdio.h>
#include "vec2d.hpp"
#include "get_time.h"

// prototype for Gram-Schmidt routine
int GramSchmidt2d(vec2d X[], int numvectors);


// Example routine to test the vec2d class
int main(int argc, char* argv[]) {
	
  int m[] = {10000, 1000, 100, 10, 100000, 1000000};
  int n[] = {1000, 10000, 100000, 1000000, 100, 10};

  //performance/validity tests (Gram-Schmidt process)

  for (int k=0; k<6; k++){
	  printf("Running GramSchmidt2d process with matrix size (%i,%i)\n", m[k], n[k]);
      vec2d Y0[] = {Random(m[k],n[k]), Random(m[k],n[k]), Random(m[k],n[k]), Random(m[k],n[k]), Random(m[k],n[k])};
      double stime = get_time();
      if (GramSchmidt2d(Y0,5))
      printf("GramSchmidt2d returned error\n");
      double ftime = get_time();
      double rtime = ftime-stime;
      printf("Resulting vectors should be orthonormal:\n");
      for (int i=0; i<5; i++)
      for (int j=i; j<5; j++)
      printf("  <Y[%i],Y[%i]> = %g\n", i, j, Dot(Y0[i], Y0[j]));
      printf("\n");
      printf("testing time: %g\n\n", rtime);
  }

  return 0;
}
