/* Lu Zhang
   SMU Mathematics
   Math 4370/6370
   19 February 2017 */

#ifndef VEC2D_B_DEFINED__
#define VEC2D_B_DEFINED__

// Inclusions
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <numeric>
#include <cmath>


// This defines a simple arithmetic vector class
class vec2d_b {

 private:

  ///// Contents /////
  long int length_m;
  long int length_n;
  double* data;

 public:

  ///// General class routines /////
  // constructor (initializes values to 0.0)
  vec2d_b(long int len_m, long int len_n);

  // destructor
  ~vec2d_b();

  // write myself to stdout
  int Write() const;

  // write myself to a file
  int Write(const char* outfile) const;

  // returns my overall length of row and column repectively
  long int Length_m() const {return length_m;};
  long int Length_n() const {return length_n;};

  // access my data array
  double* GetData() const {return data;};

  // access my data -- access value by location in array, 
  // returns handle to value so that it can be changed as well,
  // does not check for legal 'k'(k = i*length_n+j)
  double& operator[](long int k) {return data[k];};
  double operator[](long int k) const {return data[k];};


  ///// Arithmetic operations defined on a vec2d_b /////

  // in-place operations (x is the calling vec2d_b) -- 0=success, 1=fail
  int LinearSum(double a, const vec2d_b& y,      // x = a*y + b*z
                double b, const vec2d_b& z);
  int Scale(double a);                         // x = x*a
  int Copy(const vec2d_b& y);                    // x = y
  int Constant(double a);                      // x = a

  // scalar quantites derived from vectors
  double Min();                                // min x_i
  double Max();                                // max x_i

};  // end vec2d_b


// independent constructor routines
vec2d_b Linspace(double a, double b, long int m, long int n);
vec2d_b Random(long int m, long int n);

// independent arithmetic routines
double Dot(const vec2d_b& x, const vec2d_b& y);    // sum_i (x_k * y_k)
double TwoNorm(const vec2d_b& x);                // sqrt(sum_k x_k^2)
double RmsNorm(const vec2d_b& x);                // sqrt(sum_k x_k^2 / k)
double MaxNorm(const vec2d_b& x);                // max_i sum_ij |x_ij|

#endif
