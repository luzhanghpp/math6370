/* Lu Zhang
   SMU Mathematics
   Math 4370/6370
   19 February 2017 */

// Inclusions
#include <stdlib.h>
#include <stdio.h>
#include "vec2d_b.hpp"
#include "get_time.h"
#include <vector>

// prototype for Gram-Schmidt routine
int GramSchmidt2d_b(vec2d_b X[], int numvectors);

// Example routine to test the vec2d_b class
int main(int argc, char* argv[]) {
	
  int m[] = {10000, 1000, 100, 10, 100000, 1000000};
  int n[] = {1000, 10000, 100000, 1000000, 100, 10};

  //performance/validity tests (Gram-Schmidt process)
  for (int k=0; k<6; k++){
	  printf("Running GramSchmidt2d_b process with matrix size (%i,%i)\n", m[k], n[k]);
      vec2d_b Y0[] = {Random(m[k],n[k]), Random(m[k],n[k]), Random(m[k],n[k]), Random(m[k],n[k]), Random(m[k],n[k])};
      double stime = get_time();
      if (GramSchmidt2d_b(Y0,5))
      printf("GramSchmidt2d_b returned error\n");
      double ftime = get_time();
      double rtime = ftime-stime;
      printf("Resulting vectors should be orthonormal:\n");
      for (int i=0; i<5; i++)
      for (int j=i; j<5; j++)
      printf("  <Y[%i],Y[%i]> = %g\n", i, j, Dot(Y0[i], Y0[j]));
      printf("\n");
      printf("testing time: %g\n\n", rtime);
  }

  return 0;
}
