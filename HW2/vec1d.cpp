/* Daniel R. Reynolds
   SMU Mathematics
   Math 4370/6370
   10 February 2017 */

// Inclusions
#include "vec1d.hpp"


// This file implements the operations defined in the vec1d class


///// General class routines /////

// constructor (initializes values to 0.0)
vec1d::vec1d(long int len) {
  // if len is illegal, create an empty vector
  if (len < 1) {
    length = 0;
    data = NULL;
  } else {
    length = len;
    data = new double[length];
    for (long int i=0; i<length; i++)
      data[i] = 0.0;
  }
}


// destructor (frees space associated with a vec1d)
vec1d::~vec1d() {
  if (data!=NULL)
    delete[] data;
  length = 0;
}


// write myself to stdout
int vec1d::Write() const {
  // return with failure if data array isn't allocated
  if (data == NULL) {
    fprintf(stderr, "vec1d:Write error, empty data array\n");
    return 1;
  }

  // print data to screen 
  for (long int i=0; i<length; i++)
    printf("  %.17g\n",data[i]);
  printf("\n");

  // return with success
  return 0;
}


// write myself to a file
int vec1d::Write(const char *outfile) const {
  // return with failure if data array isn't allocated
  if (data == NULL) {
    fprintf(stderr, "vec1d:Write error, empty data array\n");
    return 1;
  }

  // return with failure if 'outfile' is empty
  if (strlen(outfile) < 1) {
    fprintf(stderr, "vec1d::Write error, empty outfile\n");
    return 1;
  }

  // open output file
  FILE *fptr = NULL;
  fptr = fopen(outfile, "w");
  if (fptr == NULL) {
    fprintf(stderr, "vec1d::Write error, unable to open %s for writing\n",outfile);
    return 1;
  }

  // print data to file
  for (long int i=0; i<length; i++)
    fprintf(fptr, "  %.17g\n",data[i]);
  fprintf(fptr, "\n");

  // close output file and return
  fclose(fptr);
  return 0;
}



///// Arithmetic operations defined on a given vec1d /////

// x = a*y + b*z
int vec1d::LinearSum(double a, const vec1d& y, double b, const vec1d& z) {
  // check that array sizes match
  if (y.length != length  ||  z.length != length) {
    fprintf(stderr,"vec1d::LinearSum error, vector sizes do not match\n");
    return 1;
  }
  
  // check that data is not NULL
  if (data == NULL || y.data == NULL || z.data == NULL) {
    fprintf(stderr, "vec1d::LinearSum error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length; i++)  
    data[i] = a*y.data[i] + b*z.data[i];
  return 0;
}


//   x = x*a  (scales my data by scalar a)
int vec1d::Scale(double a) {
  // check that data is not NULL
  if (data == NULL) {
    fprintf(stderr, "vec1d::Scale error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length; i++)
    data[i] *= a;
  return 0;
}


//   x = y  (copies y into x)
int vec1d::Copy(const vec1d& y) {
  // check that array sizes match
  if (y.length != length) {
    fprintf(stderr,"vec1d::Copy error, vector sizes do not match\n");
    return 1;
  }
  
  // check that data is not NULL
  if (data == NULL || y.data == NULL) {
    fprintf(stderr, "vec1d::Copy error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length; i++)
    data[i] = y.data[i];
  return 0;
}


//   x = a  (sets all entries of x to the scalar a)
int vec1d::Constant(double a) {
  // check that data is not NULL
  if (data == NULL) {
    fprintf(stderr, "vec1d::Constant error: empty data array\n");
    return 1;
  }

  // perform operation and return
  for (long int i=0; i<length; i++)
    data[i] = a;
  return 0;
}


///// scalar quantities derived from vectors /////

// min x_i
double vec1d::Min() {
  // check that my data is allocated
  if (data == NULL) {
    fprintf(stderr,"vec1d::Min error, data not allocated\n");
    return -1.0;
  }
  
  // perform operation and return
  double mn=data[0];
  for (long int i=0; i<length; i++)  
    mn = std::min(mn,data[i]);
  return mn;
}


// max x_i
double vec1d::Max() {
  // check that my data is allocated
  if (data == NULL) {
    fprintf(stderr,"vec1d::Max error, data not allocated\n");
    return -1.0;
  }
  
  // perform operation and return
  double mx=data[0];
  for (long int i=0; i<length; i++)  
    mx = std::max(mx,data[i]);
  return mx;
}



///// independent constructor routines /////

// create a vector of linearly spaced data
vec1d Linspace(double a, double b, long int n) {
  vec1d *x = new vec1d(n);
  double *xd = x->GetData();
  double h = (b-a)/(n-1);
  for (long int i=0; i<n; i++)
    xd[i] = a + h*i;
  return *x;
}


// create a vector of uniformly-distributed random data
vec1d Random(long int n) {
  vec1d *x = new vec1d(n);
  double *xd = x->GetData();
  for (long int i=0; i<n; i++)
    xd[i] = random() / (pow(2.0,31.0) - 1.0);
  return *x;
}


///// independent arithmetic routines /////

// dot-product of x and y
double Dot(const vec1d& x, const vec1d& y) {
  // check that array sizes match
  if (y.Length() != x.Length()) {
    fprintf(stderr,"Dot error, vector sizes do not match\n");
    return 0.0;
  }
  
  // perform operation and return
  double sum=0.0;
  for (long int i=0; i<x.Length(); i++)  
    sum += x[i]*y[i];
  return sum;
}



// ||x||_2
double TwoNorm(const vec1d& x) {
  double sum=0.0;
  for (long int i=0; i<x.Length(); i++)
    sum += x[i]*x[i];
  return sqrt(sum);
}


// ||x||_RMS
double RmsNorm(const vec1d& x) {
  double sum=0.0;
  for (long int i=0; i<x.Length(); i++)
    sum += x[i]*x[i];
  return sqrt(sum/x.Length());
}


// ||x||_infty
double MaxNorm(const vec1d& x) {
  double mx=0.0;
  for (long int i=0; i<x.Length(); i++)  
    mx = std::max(mx, std::abs(x[i]));
  return mx;
}
