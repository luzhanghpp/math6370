/* Daniel R. Reynolds
   SMU Mathematics
   Math 4370/6370
   10 February 2017 */

// Inclusions
#include <stdlib.h>
#include <stdio.h>
#include "vec1d.hpp"
#include "get_time.h"

// prototype for Gram-Schmidt routine
int GramSchmidt1d(vec1d X[], int numvectors);


// Example routine to test the vec1d class
int main(int argc, char* argv[]) {

  // create some vecs of length 5, and set some entries
  vec1d a(5), b(5), c(5);
  for (int i=0; i<5; i++)  b[i] = (i+1)*0.1;
  for (int i=0; i<5; i++)  c[i] = (i+1);

  // output to screen
  printf("writing array of zeros:\n");
  a.Write();
  printf("writing array of 0.1, 0.2, 0.3, 0.4, 0.5:\n");
  b.Write();
  printf("writing array of 1,2,3,4,5:\n");
  c.Write();

  // verify that b has length 5
  if (b.Length() != 5) 
    printf("error: incorrect vector length\n");

  // access a's data array, update entries, and write to file
  double *dat = a.GetData();
  dat[0] = 10.0;
  dat[1] = 15.0;
  dat[2] = 20.0;
  dat[3] = 25.0;
  dat[4] = 30.0;
  a.Write("a_data");
  printf("the new file 'a_data' on disk should have entries 10, 15, 20, 25, 30\n\n");
  
  // access each entry of a and write to screen
  printf("entries of a, one at a time (via []): should give 10, 15, 20, 25, 30\n");
  for (int i=0; i<5; i++) 
    printf("  %g\n",a[i]);
  printf("\n");

  // update one entry of a
  printf("updating the last entry of a to be 31 (via a[4]):\n");
  a[4] = 31.0;
  a.Write();
  a[4]= 30.0;  // reset to original

  // Test arithmetic operators
  printf("Testing vector constant, should all be -1\n");
  b.Constant(-1.0);
  b.Write();

  printf("Testing vector copy, should be 1, 2, 3, 4, 5\n");
  a.Copy(c);
  a.Write();

  printf("Testing scalar multiply, should be 5, 10, 15, 20, 25\n");
  c.Scale(5.0);
  c.Write();

  // create a few vecs of length 10
  vec1d X[] = {vec1d(10), vec1d(10), vec1d(10), vec1d(10), vec1d(10)};

  // fill in the vectors 
  for (int i=0; i<10; i++) {
    X[0][i] = 1.0*i;
    X[1][i] = -5.0 + 1.0*i;
    X[2][i] = 2.0 + 2.0*i;
    X[3][i] = 20.0 - 1.0*i;
    X[4][i] = -20.0 + 1.0*i;
  }
  
  // check the LinearSum routine 
  X[0].LinearSum(-2.0, X[1], 1.0, X[2]);
  printf("Testing LinearSum, should be all 12:\n");
  X[0].Write();

  // check the various scalar output routines 
  printf("Testing TwoNorm, should be 2.23607\n");
  printf("  %g\n\n", TwoNorm(b));
  
  printf("Testing RmsNorm, should be 16.5831\n");
  printf("  %g\n\n", RmsNorm(c));
  
  printf("Testing MaxNorm, should be 1\n");
  printf("  %g\n\n", MaxNorm(b));
  
  printf("Testing Min, should be 1\n");
  printf("  %g\n\n", a.Min());
  
  printf("Testing Max, should be 25\n");
  printf("  %g\n\n", c.Max());
  
  printf("Testing Dot, should be 275\n");
  printf("  %g\n\n", Dot(a, c));
  
  printf("Testing Linspace, should be 0 1 2 3 4\n");
  vec1d d = Linspace(0.0, 4.0, 5);
  d.Write();
  
  printf("Testing Random\n");
  vec1d f = Random(5);
  f.Write();


  /// performance/validity tests (Gram-Schmidt process)
  int n=1000000;
  printf("Running GramSchmidt1d process\n");
  vec1d Y[] = {Random(n), Random(n), Random(n), Random(n), Random(n)};
  double stime = get_time();
  if (GramSchmidt1d(Y,5))
    printf("GramSchmidt1d returned error\n");
  double ftime = get_time();
  double rtime = ftime-stime;
  printf("Resulting vectors should be orthonormal:\n");
  for (int i=0; i<5; i++)
    for (int j=i; j<5; j++)
      printf("  <Y[%i],Y[%i]> = %g\n", i, j, Dot(Y[i], Y[j]));
  printf("\n");
  printf("testing time: %g\n", rtime);

  return 0;
} // end main

