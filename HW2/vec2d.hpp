/* Lu Zhang
   SMU Mathematics
   Math 4370/6370
   16 February 2017 */

#ifndef VEC2D_DEFINED__
#define VEC2D_DEFINED__

// Inclusions
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <numeric>
#include <cmath>


// This defines a simple arithmetic vector class
class vec2d {

 private:

  ///// Contents /////
  long int length_m;   // the number of row
  long int length_n;   // the number of column
  double** data;

 public:

  ///// General class routines /////
  // constructor (initializes values to 0.0)
  vec2d(long int len_m,long int len_n);

  // destructor
  ~vec2d();

  // write myself to stdout
  int Write() const;

  // write myself to a file
  int Write(const char* outfile) const;

  // returns my overall length
  long int Length_m() const {return length_m;};
  long int Length_n() const {return length_n;};

  // access my data 
  double** GetData() const {return data;};

  // access my data -- access value by location in array, 
  // returns handle to value so that it can be changed as well,
  // does not check for legal 'i' and 'j'
  double& operator()(long int i, long int j) {return data[i][j];};
  double operator()(long int i, long int j) const {return data[i][j];};
  
  //double operator-(double l){return -1*l;};


  ///// Arithmetic operations defined on a vec2d /////

  // in-place operations (x is the calling vec2d) -- 0=success, 1=fail
  int LinearSum(double a, const vec2d& y,      // x = a*y + b*z
                double b, const vec2d& z);
  int Scale(double a);                         // x = x*a
  int Copy(const vec2d& y);                    // x = y
  int Constant(double a);                      // x = a

  // scalar quantites derived from vectors
  double Min();                                // min x_i
  double Max();                                // max x_i

};  // end vec2d


// independent constructor routines
vec2d Linspace(double a, double b, long int m, long int n);
vec2d Random(long int m, long int n);

// independent arithmetic routines
double Dot(const vec2d& x, const vec2d& y);    // sum x_ij*y_ij
double TwoNorm(const vec2d& x);                // sqrt(sum_ij x_ij^2)
double RmsNorm(const vec2d& x);                // sqrt(sum_ij x_ij^2 / m*n)
double MaxNorm(const vec2d& x);                // max_i sum_j|x_j|

#endif
