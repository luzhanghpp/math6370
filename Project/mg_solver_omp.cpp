/* this script is used to call vcycle to solve problem
 
   Lu Zhang
   Math 6370 @SMU
   Spring 2017                                                */


// Inclusion
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "omp.h"

using namespace std;

// declear the function prototype
vector<double> vcycle_jcobi_omp(int, int, int, double, double, vector<double>, vector<double>, int, int);

vector<double> mg_solver_omp(int kmax, int L, int mu1, int mu2, double eps, double hx, double hy, vector<double> u, vector<double> f, int Nx, int Ny){

  vector<double> r(Nx*Ny);
  vector<double> rd(Nx*Ny);
  vector<double> rold(Nx*Ny);
  double r0 = 0.0;
//  double normf;
  int k = 1;

#pragma omp parallel default(shared)
{
#pragma omp for collapse(2) schedule(static)
  for(int i=1; i<Nx-1; i++)
    for(int j=1; j<Ny-1; j++)
      // initilize the residual
      r[j*Nx+i] = f[j*Nx+i] + (2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy - u[(j+1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx;

#pragma omp for collapse(2) schedule(static)
  for(int i=0; i<Nx; i++)
    for(int j=0; j<Ny; j++)
      r0 += r[j*Nx+i]*r[j*Nx+i];   // the norm of intial residual
}// end parallel region

  //normf = sqrt(r0)*eps;            // criterior of jumping from while
                               
  while( sqrt(r0/Nx/Ny) > eps && k < kmax){
    u = vcycle_jcobi_omp(L, mu1, mu2, hx, hy, u, f, Nx, Ny);      // call vcycle to solve Au = f

  r0 = 0.0;
  
#pragma omp parallel default(shared)
{
//#pragma omp for collapse(2) schedule(static)
  //for(int i=1; i<Nx-1; i++)
    //for(int j=1; j<Ny-1; j++)
      //rold[j*Nx+i] = r[j*Nx+i];

#pragma omp for collapse(2) schedule(static)
  for(int i=1; i<Nx-1; i++)
    for(int j=1; j<Nx-1; j++)
      // calculate the new residual
      r[j*Nx+i] = f[j*Nx+i]+(2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy - u[(j+1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx;

//#pragma omp for collapse(2) schedule(static) 
  //for(int i=1; i<Nx-1; i++)
   //for(int j=1; j<Ny-1; j++)
     //rd[j*Nx+i] = r[j*Nx+i] - rold[j*Nx+i];

#pragma omp for collapse(2) schedule(static) reduction(+:r0)
  for(int i=0; i<Nx; i++)
    for(int j=0; j<Ny; j++)
      // calculate the new norm of residual
      r0 += r[j*Nx+i]*r[j*Nx+i];
}// end parallel region
                                                                                     
  k = k + 1;      // increase the iteration number
  //cout << " the norm of residual = " << sqrt(r0/Nx/Ny) << endl;
  }

  cout << " the number of iteration = " << k-1 << endl;
  return u;
}

