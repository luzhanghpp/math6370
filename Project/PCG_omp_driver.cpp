/* driver of using preconditioner conjugate gradient method to solve the possion 
   equation in 2d 
                      u_xx + u_yy = f  ax < x < bx, ay < y < by
                                u = 0  on the boundary

   Lu Zhang
   Math 6370 @ SMU
   Spring 2017 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <omp.h>
#include "get_time.h"

using namespace std;
// declaring functions prototypes
vector<double> pcg(double, double, vector<double>, vector<double>, double, int, int, int);

// main routine
int main() {

  // declearation
  double stime1, ftime1, stime2, ftime2, runtime1, runtime2;  // create variables to accept running time
  const double pi = 4.0*atan(1.0);                            // set the value of pi which will be used in the test problem
  double ax = -1.0;                                           // x in (ax,bx)
  double bx = 1.0;
  double ay = -1.0; // -1.0;                                   // y in (ay,by)
  double by = 1.0; // 1.0; 
  vector<int> Nx = {33, 65, 129, 257, 513, 1025, 2049};               // the number of grids in x direction(various choices)
  vector<int> Ny = {33, 65, 129, 257, 513, 1025, 2049};               // the number of grids in y direction(various choices)
  vector<double> error(Nx.size());                            // create error vector
  vector<double> rate(Nx.size()-1);                           // create convergent rate vector
  double eps = 1e-10;                                         // criterion of exiting the loop
  int kmax = 100000;                                          // maximum iteration number
  double err = 0.0;                                           // initilize the error

  // choose different Nx value
  for(int s=0; s<Nx.size(); s++){
    stime1 = get_time();                                      // get stime1(make sure this phase is not need to parallel)
    double hx = (bx - ax)/(Nx[s] - 1);                        // the width of cell in x direction
    double hy = (by - ay)/(Ny[s] - 1);                        // the width of cell in y direction
    vector<double> x(Nx[s]);                                  // build the vector which contains the mesh points in x line
    vector<double> y(Ny[s]);                                  // build the vector which contains the mesh points in y line
    vector<double> u(Nx[s]*Ny[s]);                            // create the numerical solution vector
    vector<double> utrue(Nx[s]*Ny[s]);                        // create the true solution vector
    vector<double> f(Nx[s]*Ny[s]);                            // create the right hand side of the system Au = f
    
    for(int i=0; i<Nx[s]; i++)                                // set the mesh points on x line
      x[i] = i*hx;                                       

    for(int j=0; j<Ny[s]; j++)                                // set the mesh points on y line
      y[j] = j*hy;                                       

    for(int i=0; i<Nx[s]; i++){                               // set the true and numerical  solution for our problem
      for(int j=0; j<Ny[s]; j++){
        utrue[j*Nx[s]+i] = sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);    
        u[j*Nx[s]+i] = 0.0;
      }
    }

    for(int i=1; i<Nx[s]-1; i++)                              // set the analytic right hand side function for our problem
      for(int j=1; j<Ny[s]-1; j++)
       f[j*Nx[s]+i] = -200.0*pi*pi*sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);
   

    ftime1 = get_time();                                      // get ftime1

    stime2 = get_time();                                      // get stime2
    u = pcg(hx, hy, u, f, eps, kmax, Nx[s], Ny[s]);           // call pcg function to solve the linear system
    ftime2 = get_time();

    for(int k=0; k<Nx[s]*Ny[s]; k++)
      err += (u[k] - utrue[k])*(u[k] - utrue[k]);             // get the numerical err    
    err = sqrt(err/Nx[s]/Ny[s]);                              // normalize our err for different Nx,Ny
    error[s] = err;

    runtime1 = ftime1 - stime1;                               // get the value of runtime
    runtime2 = ftime2 - stime2;
     
    // output error and convergent rate
    if(s==0)
      cout << "hx = " << (bx - ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1) << "\t error = " << error[s] << "\t runtime1 = " << runtime1  <<"\t runtime2 = " << runtime2  << endl;
    else{
      //compute the convergent rate
      rate[s-1] =log(error[s]/error[s-1])/log(sqrt((bx-ax)*(bx-ax)/(Nx[s]-1)/(Nx[s]-1)+(by-ay)*(by-ay)/(Ny[s]-1)/(Ny[s]-1))/sqrt((bx-ax)*(bx-ax)/(Nx[s-1]-1)/(Nx[s-1]-1)+(by-ay)*(by-ay)/(Ny[s-1]-1)/(Ny[s-1]-1)));
      cout << "hx = " << (bx-ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1)<< "\t error = " << error[s] << "\t cov_rate = " << rate[s-1] <<"\t runtime1 = " << runtime1 << "\t runtime2 = " << runtime2 << endl;
    }// end else

  }// end s loop

  return 0;
}



