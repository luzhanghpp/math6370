/* driver of using multigrid to solve the possion equation in 2d
                    u_xx + u_yy = f  ax < x < bx, ay < y < by
                              u = 0 on the boundary     

   Lu Zhang
   Math 6370 @ SMU
   Spring 2017 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "get_time.h"

using namespace std;

// declaring functions prototypes
//vector<double> mg_solver_omp(int, int, int, int, double, double, double, vector<double>, vector<double>, int, int);
vector<double> vcycle_jcobi_omp(int, int, int, double,double, vector<double>, vector<double>, int, int);
int main() {

  // declearation
  double stime1,ftime1,stime2,ftime2,runtime1,runtime2;    // set variable to receive the running time
  const double pi = 4.0*atan(1.0);                         // define the value of pi(for test our method)
  double ax = -1.0;                                        // x in (ax, bx)
  double bx = 1.0;
  double ay = -1.0;                                        // y in (ay, by)
  double by = 1.0;
  vector<int> Nx ={33, 65, 129, 257, 513, 1025, 2049};  // Nx is the number of meshpoints in x line(contain end points)
  vector<int> Ny = {33, 65, 129, 257, 513, 1025, 2049};  // Ny is the number of meshpoints in y line
  vector<double> error(Nx.size());                         // set vector to receive error
  vector<double> rate(Nx.size()-1);                        // set vector to receive convergent rate

  // choose different (Nx,Ny)
  for(int s=0; s<Nx.size(); s++){
    
    stime1 = get_time();                                   // get the value of stime1 for different s loop
    double hx = (bx - ax)/(Nx[s] - 1.0);                   // the cell width in x direction
    double hy = (by - ay)/(Ny[s] - 1.0);                   // the cell width in y direction
    vector<double> x(Nx[s]);                               // get the meshpoints in x line
    vector<double> y(Ny[s]);                               // get the meshpoints in y line
    vector<double> utrue(Nx[s]*Ny[s]);                     // set the true solution
    vector<double> u(Nx[s]*Ny[s]);                         // set the numerical solution
    vector<double> f(Nx[s]*Ny[s]);                         // set the right hand side of equation Au = f
    int L = min(log10(Nx[s]-1)/log10(2.0),log10(Ny[s]-1)/log10(2.0));  // number of levels in mg
    int mu1 = 10;                                           // set the number of pre--smoothing
    int mu2 = 10;                                           // set the number of post--smoothing
    int kmax = 100000;
    int vn = 20;
    double eps = 1e-4;
    double err = 0.0;                                      // initialize the error

    for(int i=0; i<Nx[s]; i++)
      x[i] = i*hx;                                         // set mesh points in x direction

    for(int j=0; j<Ny[s]; j++)
      y[j] = j*hy;                                         // set mesh points in y direction
      
    for(int i = 1; i < Nx[s]-1 ; i++)
      for(int j = 1; j < Ny[s]-1 ; j++){                   // initialize the numerical, true solution and rhs
        utrue[j*Nx[s] + i] = sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);
        f[j*Nx[s]+i] = -200.0*pi*pi*sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);      
        u[j*Nx[s]+i] = 0.0;                         
      }
    
    ftime1 = get_time();                                   // get ftime1
	      
    stime2 = get_time();
    for(int i=0; i<vn; i++)                                   // get stime2
      u = vcycle_jcobi_omp(L, mu1, mu2, hx, hy, u, f, Nx[s], Ny[s]);
     // u = mg_solver_omp(kmax,L, mu1,mu2,eps,hx,hy,u,f,Nx[s],Ny[s]);

    ftime2 =get_time();

    for(int k=0; k<Nx[s]; k++)
      for(int l=0; l<Ny[s]; l++)                           // calculate error
        err = err + (u[l*Nx[s]+k] - utrue[l*Nx[s]+k])*(u[l*Nx[s]+k] - utrue[l*Nx[s]+k]);
    error[s] = sqrt(err/Nx[s]/Ny[s]);                      // normalize our error for different Nx and Ny 

    runtime1 = ftime1 - stime1;
    runtime2 = ftime2 - stime2;     

    // output error and convergent rate
    if(s==0)
      cout << "hx = " << (bx - ax)/(Nx[s]-1) << "\t hy = " << (by - ay)/(Ny[s] - 1) << "\t error = " << error[s] << "\t runtime1 = " << runtime1 << "\t runtime2 = " << runtime2 <<endl;
    else{
      //compute the convergent rate
      rate[s-1] =log(error[s]/error[s-1])/log(sqrt((bx-ax)*(bx-ax)/(Nx[s]-1)/(Nx[s]-1)+(by-ay)*(by-ay)/(Ny[s]-1)/(Ny[s]-1))/sqrt((bx-ax)*(bx-ax)/(Nx[s-1]-1)/(Nx[s-1]-1)+(by-ay)*(by-ay)/(Ny[s-1]-1)/(Ny[s-1]-1)));
      cout << "hx = " << (bx-ax)/(Nx[s]-1) << "\t hy = " << (by - ay)/(Ny[s] - 1)<< "\t error = " << error[s] << "\t cov_rate = " << rate[s-1] << "\t runtime1 = " << runtime1 << "\t runtime2 = " << runtime2  << endl;
    }
  }// end for Nx and Ny choice
  return 0;
}








 
