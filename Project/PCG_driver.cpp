/* driver of using preconditioner conjugate gradient method to solve the possion 
   equation in 2d 
                      u_xx + u_yy = f  ax < x < bx, ay < y < by
                                u = 0  on the boundary

   Lu Zhang
   Math 6370 @ SMU
   Spring 2017 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

// declaring functions prototypes
vector<double> pcg(double, double, vector<double>, vector<double>, double, int, int, int);

// main routine
int main() {

  // set the value of pi which will be used in the test problem
  const double pi = 4.0*atan(1.0);

  // domain of the test problem x in (ax,bx), and y in (ay,by)
  double ax = -1.0;
  double bx = 1.0;
  double ay = 1.0; // -1.0;
  double by = 2.0; // 1.0;

  // the number of grids in x direction and y direction respectively 
  vector<int> Nx = {33, 65, 129, 257, 513};
  vector<int> Ny = {33, 65, 129, 257, 513};

  // set vector to recieve error and convergence rate
  vector<double> error(Nx.size());
  vector<double> rate(Nx.size()-1);

  for(int s=0; s<Nx.size(); s++){

    // the width of cell in each direction
    double hx = (bx - ax)/(Nx[s] - 1);
    double hy = (by - ay)/(Ny[s] - 1);

    // criterion of exiting the loop
    double eps = 1e-15;

    // maximum iteration number
    int kmax = 100000;

    // build the mesh points in x line
    vector<double> x(Nx[s]);
    for(int i=0; i<Nx[s]; i++)
      x[i] = i*hx;

    // build the mesh points in y line
    vector<double> y(Ny[s]);
    for(int j=0; j<Ny[s]; j++)
      y[j] = j*hy;

    // set the initial guess to 0 vector and true solution of problem
    vector<double> u(Nx[s]*Ny[s]);
    vector<double> utrue(u);
    for(int i=0; i<Nx[s]; i++)
      for(int j=0; j<Ny[s]; j++){
        utrue[j*Nx[s]+i] = sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);
        u[j*Nx[s]+i] = 0.0;
      }

    // set the right hand side of linear system Au = f
    vector<double> f(u);
    for(int i=1; i<Nx[s]-1; i++)
      for(int j=1; j<Ny[s]-1; j++)
	f[j*Nx[s]+i] = -200.0*pi*pi*sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);

    // call pcg function to solve the linear system
    u = pcg(hx, hy, u, f, eps, kmax, Nx[s], Ny[s]);

    // get the error of our method
    double err = 0.0;
    for(int k=0; k<Nx[s]*Ny[s]; k++)
      err += (u[k] - utrue[k])*(u[k] - utrue[k]);

    // normalize our err for different Nx,Ny
    err = sqrt(err/Nx[s]/Ny[s]);

    error[s] = err;
    // output error and convergent rate
    if(s==0)
      cout << "hx = " << (bx - ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1) << "\t    error = " << error[s] << endl;
    else{
      //compute the convergent rate
      rate[s-1] =log(error[s]/error[s-1])/log(sqrt((bx-ax)*(bx-ax)/(Nx[s]-1)/(Nx[s]-1)+(by-ay)*(by-ay)/(Ny[s]-1)/(Ny[s]-1))/sqrt((bx-ax)*(bx-ax)/(Nx[s-1]-1)/(Nx[s-1]-1)+(by-ay)*(by-ay)/(Ny[s-1]-1)/(Ny[s-1]-1)));
      // rate[s-1] =log(error[s]/error[s-1])/log(((bx-ax)/(Nx[s]-1))/((bx-ax)/(Nx[s-1]-1)));
      cout << "hx = " << (bx-ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1)<< "\t    error = " << error[s] << "\t       cov_rate = " << rate[s-1] << endl;
    }

  }// end the choice of Nx

  return 0;
}
