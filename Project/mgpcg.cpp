/* Lu  Zhang @ SMU
   Math 6370
   Spring 2017     */

// Inclusions
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

/* this function used to get the solution of linear system Au = f by MGPCG
   the stencil for each point is
                     | 0             1/hy/hy                 0 |
                     | 1/hx/hx    -2/hx/hx - 2/hy/hy   1/hx/hx |
                     | 0             1/hy/hy                 0 |       
   input:
           hx is the cell width in x line
           hy is the cell width in y line
           u is the initial guess of the linear system Au = f
           f is the right hand side of linear system
           eps and kmax determine the stopping criterium
           Nx is the number of grids in x line
           Ny is the number of grids in y line
           nv is the number of calling v cycle
   output:
           u is the solution of the linear system    */

// declaring function prototype
vector<double> vcycle_jcobi(int, int, int, double, double, vector<double>, vector<double>, int, int);

vector<double> mgpcg(double hx, double hy, vector<double> u, vector<double> f, double eps, int kmax, int Nx, int Ny, int nv,int mu1, int mu2){

  // get the nember of levels for multigrid
  int L = min(log10(Nx-1)/log10(2.0), log10(Ny-1)/log10(2.0));

  // initialize residual r = f - laplace u
  vector<double> r(f);
  for(int i=1; i<Nx-1; i++)
      for(int j=1; j<Ny-1; j++)
	r[j*Nx+i] += (2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx-u[(j+1)*Nx+i]/hy/hy;

  // initialize the norm of residual
  double r0 = 0.0;
  for(int i=0; i<Nx; i++)
    for(int j=0; j<Ny; j++)
      r0 += r[j*Nx+i]*r[j*Nx+i];

  // initialize the number of iteration
  int k = 1;

  // declare some variable which will be used later
  vector<double> p(u);
  double tauprev;
  vector<double> w(u);
  double alpha;
  

  // precompute the bound of residual size
  double normb = sqrt(r0)*eps;
     
  // start the loop
  while( sqrt(r0) > normb && k < kmax){

    // action of preconditioner(use multigrid to be the preconditioner)
    // initialize z
    vector<double> z(Nx*Ny);
    for(int k=0; k<Nx*Ny; k++)
      z[k] = 0.0;

    // use multigrid to get z
    for(int j=0; j < nv; j++)
    z = vcycle_jcobi(L, mu1, mu2, hx, hy, z, r, Nx, Ny);

    // compute new coefficient
    double tau = 0.0;
    for(int i=0; i<Nx; i++)
      for(int j=0; j<Ny; j++)
	tau += r[j*Nx+i]*z[j*Nx+i];
   
    if(k==1){
      for(int k=0; k<Nx*Ny; k++)
	p[k] = z[k];
    }
    else{
      double beta = tau/tauprev;
      // new search direction
      for(int k=0; k<Nx*Ny; k++)
	p[k] = z[k] + beta*p[k];
    }

    // auxiliary vector
    for(int i=1; i<Nx-1; i++)
      for(int j=1; j<Ny-1; j++)
	w[j*Nx+i] = (-2/hx/hx - 2/hy/hy)*p[j*Nx+i] + p[(j-1)*Nx+i]/hy/hy + p[j*Nx+i-1]/hx/hx + p[j*Nx+i+1]/hx/hx + p[(j+1)*Nx+i]/hy/hy;

    // find optimal alpha in line search
    double dot = 0.0;
    for(int k=0; k<Nx*Ny; k++)
      dot += p[k]*w[k];   
    alpha = tau/dot;

    // new guess
    for(int k=0; k<Nx*Ny; k++)
      u[k] += alpha*p[k];

    // recompute residual
    for(int k=0; k<Nx*Ny; k++)
      r[k] -= alpha*w[k];

    // save previous coefficient
    tauprev = tau;

    // compute norm of new residual
    double r0_temp = 0.0;
    for(int k=0; k<Nx*Ny; k++)
      r0_temp += r[k]*r[k];

    r0 = r0_temp;

    // increase number of iterations
    k = k + 1;
  }
  cout << " use " << k << " iterations" << endl;
  return u;
}



    
