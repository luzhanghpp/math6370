/* driver of using multigrid preconditioner conjugate gradient method to solve the possion 
   equation in 2d 
                         u_xx + u_yy = f  ax < x < bx, ay < y < by
                                   u = 0  on the boundary

   Lu Zhang
   Math 6370 @ SMU
   Spring 2017 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "get_time.h"

using namespace std;

// declaring functions prototypes
vector<double> mgpcg_omp(double, double, vector<double>, vector<double>, double, int, int, int, int,int,int);

// main routine
int main() {

  //declearation
  double stime1,ftime1,stime2,ftime2,runtime1,runtime2;       // set variables to receive running time
  const double pi = 4.0*atan(1.0);                            // set the value of pi which will be used in the test problem
  int mu1 = 10;                                                // set the number of calling pre--smoother function
  int mu2 = 10;                                                // set the number of calling post--smoother function
  int nv = 20;                                                // set the number of calling v-cycle
  double ax = -1.0;                                           // x in (ax,bx)
  double bx = 1.0;
  double ay = -1.0;                                   // y in (ay,by)
  double by = 1.0;
  double eps = 1e-10;                                         // criterion of exiting the loop
  int kmax = 100000;                                          // maximum iteration number
  vector<int> Nx = {33, 65, 129, 257, 513, 1025, 2049,4097,8193};               // the number of grids in x line
  vector<int> Ny = {33, 65, 129, 257, 513, 1025, 2049,4097,8193};               // the number of grids in y line
  vector<double> error(Nx.size());                            // set vector to receive the error 
  vector<double> rate(Nx.size() - 1);                         // set vector to receive convergent rate

  // choose different pair (Nx,Ny)
  for(int s=0; s<Nx.size(); s++){
    
    stime1 = get_time();                                      // get stime1 for different choices of s
    double hx = (bx - ax)/(Nx[s]-1);                          // the width of cell in x direction
    double hy = (by - ay)/(Ny[s]-1);                          // the width of cell in y direction
    vector<double> x(Nx[s]);                                  // build the mesh points in x line
    vector<double> y(Ny[s]);                                  // build the mesh points in y line
    vector<double> u(Nx[s]*Ny[s]);                            // set the initial guess 
    vector<double> utrue(Nx[s]*Ny[s]);                        // set the true solution
    vector<double> f(Nx[s]*Ny[s]);                            // set the right hand side function Au = f
    double err = 0.0;                                         // initialize the error to 0

    for(int i=0; i<Nx[s]; i++)
      x[i] = i*hx;                                            // set mesh points in x line

    for(int j=0; j<Ny[s]; j++)
      y[j] = j*hy;                                            // set mesh points in y line

    for(int i=1; i<Nx[s]-1; i++)                              // set the true, numerical solution and rhs for our problem
      for(int j=1; j<Ny[s]-1; j++){ 
        utrue[j*Nx[s]+i] = sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);
        f[j*Nx[s]+i] = -200.0*pi*pi*sin(10.0*pi*x[i])*sin(10.0*pi*y[j]);
        u[j*Nx[s]+i] = 0.0;                
      }

    ftime1 = get_time();                                      // get ftime1       
    stime2 = get_time();                                      // get stime2
    // call pcg function to solve the linear system
    u = mgpcg_omp(hx, hy, u, f, eps, kmax, Nx[s], Ny[s], nv, mu1, mu2);
    ftime2 = get_time();                                      // get ftime2

    for(int k=0; k<Nx[s]*Ny[s]; k++)
      err += (u[k] - utrue[k])*(u[k] - utrue[k]);             // get the error of numerical solution
    err = sqrt(err/Nx[s]/Ny[s]);                              // normalize our error for different choice of Nx and Ny
    error[s] = err;

    runtime1 = ftime1 - stime1;
    runtime2 = ftime2 - stime2;

    if(s==0)
      cout << "hx = " << (bx - ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1) << "\t error = " << error[s] << "\t runtime1 = " << runtime1 << "\t runtime2 = " << runtime2 << endl;
    else{
      //compute the convergent rate
      rate[s-1] =log(error[s]/error[s-1])/log(sqrt((bx-ax)*(bx-ax)/(Nx[s]-1)/(Nx[s]-1)+(by-ay)*(by-ay)/(Ny[s]-1)/(Ny[s]-1))/sqrt((bx-ax)*(bx-ax)/(Nx[s-1]-1)/(Nx[s-1]-1)+(by-ay)*(by-ay)/(Ny[s-1]-1)/(Ny[s-1]-1)));
      cout << "hx = " << (bx-ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1)<< "\t error = " << error[s] << "\t cov_rate = " << rate[s-1] << "\t runtime1 = " << runtime1 << "\t runtime2 = " << runtime2 << endl;
    }// end else
  }// end the choices of Nx

  return 0;
}
