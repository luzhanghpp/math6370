/* Lu  Zhang @ SMU
   Math 6370
   Spring 2017     */

// Inclusions
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

/* this function is used to generate the gauss seidel iterative method
   use finite difference method to discretize the laplace operator in 2d, and have following form
                              | 0                1/hy/hy                0 |
                              | 1/hx/hx     -2/hx/hx - 2/hy/hy     1/hx/hx|
                              | 0                1/hy/hy                0 |
   input:
           hx is the cell width in x direction
           hy is the cell width in y direction
           u is the initial guess for Au = f
           f is the right hand side of linear system
           Nx is the number of grids in x direction(include end points)
           Ny is the number of grids in y direction(include end points)
   output:
           u is the simulation results of linear system  */

vector<double> gauss_seidel(double hx, double hy, vector<double> u, vector<double> f, int Nx, int Ny){

  // u^m(k) = (f(k) - sum_(l<k) A_kl*u^m(l) - sum_(l>k) A_kl*u^(m-1)(l))/A_kk
  for(int i=1; i<Nx-1; i++)
    for(int j=1; j<Ny-1; j++)
      u[j*Nx+i] = (f[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx-u[(j+1)*Nx+i]/hy/hy)/(-2.0/hx/hx-2.0/hy/hy);
       
  return u;
}
