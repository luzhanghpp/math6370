/* driver of using multigrid to solve the possion equation in 2d
                    u_xx + u_yy = f  ax < x < bx, ay < y < by
                              u = 0 on the boundary     

   Lu Zhang
   Math 6370 @ SMU
   Spring 2017 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

// declaring functions prototypes
vector<double> vcycle_gs(int, int, int, double, double, vector<double>, vector<double>, int, int);

// main routine
int main() {

  // x in (ax, bx)
  double ax = -1.0;
  double bx = 1.0;

  // y in (ay, by)
  double ay = -1.0;
  double by = 1.0;

  // Nx is the number of meshpoints in x line, Ny is the number of meshpoints in y line
  // both of them cotain the end points
  //vector<int> Nx = {257, 513, 1025, 2049, 4097, 8193, 16385};
  //vector<int> Ny = {257, 513, 1025, 2049, 4097, 8193, 16385};
  vector<int> Nx = {33, 65, 129, 257, 523, 1025, 2049};
  vector<int> Ny = {33, 65, 129, 257, 523, 1025, 2049};


  // set vectors to receive error and convergence rate of the method
  vector<double> error(Nx.size());
  vector<double> rate(Nx.size()-1);

  for(int s=0; s<Nx.size(); s++){

    // calculate the cell width
    double hx = (bx - ax)/(Nx[s] - 1.0);
    double hy = (by - ay)/(Ny[s] - 1.0);

    // define the value of pi(for test our method)
    const double pi = 4.0*atan(1.0);

    // get the meshpoints in x line
    vector<double> x(Nx[s]);
    for(int i=0; i<Nx[s]; i++)
      x[i] = i*hx;

    // get the meshpoints in y line
    vector<double> y(Ny[s]);
    for(int j=0; j<Ny[s]; j++)
      y[j] = j*hy;
 
    // initialize the numerical results to 0 and also set the ture solution to problem
    // set utrue = 0 at boundary
    vector<double> utrue(Nx[s]*Ny[s]);
    vector<double> u(Nx[s]*Ny[s]);
    for(int i = 1; i < Nx[s]-1 ; i++)
      for(int j = 1; j < Ny[s]-1 ; j++){
        utrue[j*Nx[s] + i] = sin(100.0*pi*x[i])*sin(100.0*pi*y[j]);
        u[j*Nx[s]+i] = 0.0;
      }

    // calculate the maximum number of level which is used in vcycle
    int L = min(log10(Nx[s]-1)/log10(2.0), log10(Ny[s]-1)/log10(2.0));

    // set the number of pre-smoothing mu1 and the number of post-smoothing mu2
    int mu1 = 1;
    int mu2 = 1;
  
    // build the right hand side of our problem
    // also set f equal 0 at boundary
    vector<double> f(u);
    for(int i=1; i<Nx[s]-1; i++)
      for(int j=1; j<Ny[s]-1; j++)
	f[j*Nx[s]+i] = -20000.0*pi*pi*sin(100.0*pi*x[i])*sin(100.0*pi*y[j]);
        

    // set the number of vcycle for our problem and vectors to receive error and convergent rate respectively
    int vn = 10;

    // call multigrid vcycle to get our numerical solution
    for(int j=0; j < vn; j++)
      u = vcycle_gs(L, mu1, mu2, hx, hy, u, f, Nx[s], Ny[s]);

    //initialize the error at jth vcycle
    double err = 0.0;
    for(int k=0; k<Nx[s]; k++)
      for(int l=0; l<Ny[s]; l++)
        err = err + (u[l*Nx[s]+k] - utrue[l*Nx[s]+k])*(u[l*Nx[s]+k] - utrue[l*Nx[s]+k]);

    // normalize our error for different Nx and Ny
    error[s] = sqrt(err/Nx[s]/Ny[s]);

    // output error and convergent rate
    if(s==0)
      cout << "hx = " << (bx - ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1) << "\t    error = " << error[s] << endl;
    else{
      //compute the convergent rate
      //rate[s-1] =log(error[s]/error[s-1])/log(sqrt((bx-ax)*(bx-ax)/(Nx[s]-1)/(Nx[s]-1)+(by-ay)*(by-ay)/(Ny[s]-1)/(Ny[s]-1))/sqrt((bx-ax)*(bx-ax)/(Nx[s-1]-1)/(Nx[s-1]-1)+(by-ay)*(by-ay)/(Ny[s-1]-1)/(Ny[s-1]-1)));
       rate[s-1] =log(error[s]/error[s-1])/log(((bx-ax)/(Nx[s]-1))/((bx-ax)/(Nx[s-1]-1)));
      cout << "hx = " << (bx-ax)/(Nx[s]-1) << "\t  hy = " << (by - ay)/(Ny[s] - 1)<< "\t    error = " << error[s] << "\t       cov_rate = " << rate[s-1] << endl;
    }
  }// end for Nx and Ny choice
  return 0;
}



  



  
  

  
