/* Lu  Zhang @ SMU
   Math 6370
   Spring 2017     */

// Inclusions
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <omp.h>

using namespace std;

/* this function is used to generate the vcycle of multigrid method
   input:  
           L is the number of levels of MG
           mu1 is the number of iteration of pre-smoothing
           mu2 is the number of iteration of post-smoothing
           hx is the width of cell in x direction
           hy is the width of cell in y direction
           u is the initial guess for linear system
           f is the right hand side of linear system
           Nx is the number of points in x direction of current level(contain the end points)
           Ny is the number of points in y direction of current level(contain the end points)
   output: 
           u is the numerical solution of the linear system(Au = f)   */

// declaring functions prototypes
vector<double> gauss_seidel_omp(double, double, vector<double>, vector<double>, int, int);

vector<double> vcycle_gs_omp(int L, int mu1, int mu2, double hx, double hy, vector<double> u, vector<double> f,int Nx, int Ny){
  
  
  // declearation
  vector<double> r(Nx*Ny);             // set residual of fine level
  vector<double> w(Nx*Ny);             // error on the fine grid
  int ii, jj;                          // the position of grids on the fine level
  
  // direct solve
  if(L==1)
    u[5] = (f[5] - u[2]/hy/hy - u[4]/hx/hx - u[6]/hx/hx - u[8]/hy/hy)/(-2.0/hx/hx-2.0/hy/hy);
  else{ 
    for(int k=0;k<mu1;k++)              // pre--smoothing
      u = gauss_seidel_omp(hx, hy, u, f, Nx, Ny);

    int NCx = (Nx+1)/2;                 // get the number of coarse grids at x line
    int NCy = (Ny+1)/2;                 // get the number of coarse grids at y line
    vector<double> R(NCx*NCy);          // set residual ofcoarse level
    vector<double> W(NCx*NCy);          // set the error guess on the coarse level

#pragma omp parallel default(shared) private(ii,jj)
    {
#pragma omp for collapse(2) schedule(static)
      for(int i=0; i<Nx; i++)
        for(int j=0; j<Ny; j++){
          //nthreads = omp_get_num_threads();
	  r[j*Nx+i] = f[j*Nx+i];         // set the residual to be f
	}

#pragma omp for collapse(2) schedule(static)
      for(int i=1; i<Nx-1; i++)
        for(int j=1; j<Ny-1; j++)
	  // set r = f - Au
	  r[j*Nx+i] += (2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx-u[(j+1)*Nx+i]/hy/hy;

    // use full weihting to get the residual matrix at coarse level
    //                    1/16*| 1     2      1 |
    //                         | 2     4      2 |
    //                         | 1     2      1 |
#pragma omp for collapse(2) schedule(static)
      for(int I=1; I<NCx-1; I++){
        for(int J=1; J<NCy-1; J++){
	  ii = 2*I;
	  jj = 2*J;
	  // use full weighting to get the residual at coarse level
	  R[J*NCx+I] = 1.0/16.0*(r[(jj-1)*Nx+ii-1]+2.0*r[(jj-1)*Nx+ii]+r[(jj-1)*Nx+ii+1]+2.0*r[jj*Nx+ii-1]+4.0*r[jj*Nx+ii]+2.0*r[jj*Nx+ii+1]+r[(jj+1)*Nx+ii-1]+2.0*r[(jj+1)*Nx+ii]+r[(jj+1)*Nx+ii+1]);
	}
      }
      
#pragma omp for schedule(static)      
      for(int i=0; i < NCx*NCy; i++)
	  W[i] = 0.0;                    // initialize the error at coarse level to zero
    }// end the first parallel region

    // call vcycle at coarse level
    W = vcycle_gs_omp(L-1, mu1, mu2, 2.0*hx, 2.0*hy, W, R, NCx, NCy);

    // prolongate W to the fine grid w(using bilinear interpolation)
    // the stencil for this will be
    //                    1/4*| 1      2        1 |
    //                        | 2      4        2 |
    //                        | 1      2        1 |
#pragma omp parallel default(shared) private(ii, jj)
    {
#pragma omp for collapse(2) schedule(static)
      for(int I=0; I<NCx-1; I++){
        for(int J=0; J<NCy-1; J++){
          ii = 2*I;
	  jj = 2*J;
 	  w[jj*Nx+ii] = W[J*NCx+I];
	  w[jj*Nx+ii+1] = 1.0/2.0*(W[J*NCx+I]+W[J*NCx+I+1]);
	  w[(jj+1)*Nx+ii] = 1.0/2.0*(W[J*NCx+I]+W[(J+1)*NCx+I]);
	  w[(jj+1)*Nx+ii+1] = 1.0/4.0*(W[J*NCx+I]+W[J*NCx+I+1]+W[(J+1)*NCx+I]+W[(J+1)*NCx+I+1]);
        }
      }

#pragma omp for collapse(2) schedule(static)
      for(int i = 0; i<Nx; i++)
        for(int j = 0; j<Ny; j++)
	  u[j*Nx+i] += w[j*Nx+i];                         // correct our fine grid solution
    }// end the second parallel region
    
    for(int k=0; k < mu2; k++)
      u = gauss_seidel_omp(hx, hy, u, f, Nx, Ny);         // post--smoothing
  }// end else

  //cout << " the number of threads: " << nthreads << endl;
  return u;
}
