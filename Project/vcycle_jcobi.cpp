/* Lu  Zhang @ SMU
   Math 6370
   Spring 2017     */

// Inclusions
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;
   
/* this function is used to generate the vcycle of multigrid method
    input: 
           L is the number of levels of MG
           mu1 is the number of iteration of pre-smoothing
           mu2 is the number of iteration of post-smoothing
           hx is the width of cell in x direction
           hy is the width of cell in y direction
           u is the initial guess for linear system
           f is the right hand side of linear system
           Nx is the number of points in x direction of current level(contain the end points)
           Ny is the number of points in y direction of current level(contain the end points)
   output: 
           u is the numerical solution of the linear system(Au = f) */

// declaring function prototypes
vector<double> Jacobi_iterative(double, double, vector<double>, vector<double>, int, int);

vector<double> vcycle_jcobi(int L, int mu1, int mu2, double hx, double hy, vector<double> u, vector<double> f,int Nx, int Ny){
  
  // direct solve
  if(L==1)
    u[5] = (f[5] - u[2]/hy/hy - u[4]/hx/hx - u[6]/hx/hx - u[8]/hy/hy)/(-2.0/hx/hx-2.0/hy/hy);
  else{
    
    // pre--smoothing
    for(int k=0;k<mu1;k++)
      u = Jacobi_iterative(hx, hy, u, f, Nx, Ny);

    // get the residual at current level(r = f - Au)
    vector<double> r(f);
    for(int i=1; i<Nx-1; i++)
      for(int j=1; j<Ny-1; j++)
	r[j*Nx+i] += (2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx-u[(j+1)*Nx+i]/hy/hy;
		      		      
    // restrict the residual to the coarse level
    // get the number of coarse grids at x line and y line respectively
    int NCx = (Nx+1)/2;
    int NCy = (Ny+1)/2;

    // use full weihting to get the residual matrix at coarse level
    //                    1/16*| 1     2      1 |
    //                         | 2     4      2 |
    //                         | 1     2      1 |
    vector<double> R(NCx*NCy);
    for(int I=1; I<NCx-1; I++){
      int i = 2*I;
      for(int J=1; J<NCy-1; J++){
	int j = 2*J;	
	R[J*NCx+I] = 1.0/16.0*(r[(j-1)*Nx+i-1]+2.0*r[(j-1)*Nx+i]+r[(j-1)*Nx+i+1]+2.0*r[j*Nx+i-1]+4.0*r[j*Nx+i]+2.0*r[j*Nx+i+1]+r[(j+1)*Nx+i-1]+2.0*r[(j+1)*Nx+i]+r[(j+1)*Nx+i+1]);
      }
    }
			       
    // initialize the error at coarse level to zero
    vector<double> W(NCx*NCy);
    for(int i=0; i < NCx*NCy; i++)
	W[i] = 0.0;

    // call vcycle at coarse level
    W = vcycle_jcobi(L-1, mu1, mu2, 2.0*hx, 2.0*hy, W, R, NCx, NCy);

    // prolongate W to the fine grid w(using bilinear interpolation)
    // the stencil for this will be
    //                    1/4*| 1      2        1 |
    //                        | 2      4        2 |
    //                        | 1      2        1 |
    vector<double> w(Nx*Ny);
    for(int I=0; I<NCx-1; I++){
      int i = 2*I;
      for(int J=0; J<NCy-1; J++){
	int j = 2*J;
	w[j*Nx+i] = W[J*NCx+I];
	w[j*Nx+i+1] = 1.0/2.0*(W[J*NCx+I]+W[J*NCx+I+1]);
	w[(j+1)*Nx+i] = 1.0/2.0*(W[J*NCx+I]+W[(J+1)*NCx+I]);
	w[(j+1)*Nx+i+1] = 1.0/4.0*(W[J*NCx+I]+W[J*NCx+I+1]+W[(J+1)*NCx+I]+W[(J+1)*NCx+I+1]);
      }
    }

    // correct our fine grid solution
    for(int i = 0; i<Nx; i++)
      for(int j = 0; j<Ny; j++)
	u[j*Nx+i] += w[j*Nx+i];

    // post--smoothing
    for(int k=0; k < mu2; k++)
      u = Jacobi_iterative(hx, hy, u, f, Nx, Ny);
  }

  return u;
}
  
