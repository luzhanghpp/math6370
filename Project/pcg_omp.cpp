/* Lu  Zhang @ SMU
   Math 6370
   Spring 2017     */

// Inclusions
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <omp.h>

using namespace std;

/* this function used to get the solution of linear system Au = f by PCG
   the stencil for each point is
                     | 0             1/hy/hy                 0 |
                     | 1/hx/hx    -2/hx/hx - 2/hy/hy   1/hx/hx |
                     | 0             1/hy/hy                 0 |       
   input:
           hx is the cell width in x line
           hy is the cell width in y line
           u is the initial guess of the linear system Au = f
           f is the right hand side of linear system
           eps and kmax determine the stopping criterium
           Nx is the number of grids in x line
           Ny is the number of grids in y line
   output:
           u is the solution of the linear system      */

vector<double> pcg(double hx, double hy, vector<double> u, vector<double> f, double eps, int kmax, int Nx, int Ny){

  // declearion
  vector<double> r(Nx*Ny);                                    
  vector<double> p(Nx*Ny);
  vector<double> w(Nx*Ny);
  vector<double> z(Nx*Ny);                                    
  double tauprev, alpha, normb, beta, tau, dot, r0_temp;
  double r0 = 0.0;                                   // initialize the norm of residual
  int k = 1;                                         // initialize the number of iteraton 
//  int nthreads;
                                                  
#pragma omp parallel default(shared)                 // start openmp region
  { 
#pragma omp for collapse(2) schedule(static)
    for(int i=0; i<Nx; i++){
      for(int j=0; j<Ny; j++){
   //     nthreads = omp_get_num_threads();
        r[j*Nx+i] = f[j*Nx+i];                       // initialize residual r = f 
        p[j*Nx+i] = u[j*Nx+i];                       // initialize p
        w[j*Nx+i] = u[j*Nx+i];                       // initialize w
       }// end j
    }//end i also parallel for region
  
#pragma omp for collapse(2) schedule(static)
    for(int i=1; i<Nx-1; i++)
      for(int j=1; j<Ny-1; j++)
	// r = f - laplace u
	r[j*Nx+i] += (2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx-u[(j+1)*Nx+i]/hy/hy;    

#pragma omp for schedule(static) reduction(+:r0)
    for(int i=0; i<Nx; i++)
      for(int j=0; j<Ny; j++)
        r0 += r[j*Nx+i]*r[j*Nx+i];                  // get the norm of residual

//#pragma omp single
   // normb = sqrt(r0)*eps;                           // get the cretierior to skip the loop

    // start the loop
    while( sqrt(r0/Nx/Ny) > eps && k < kmax){

#pragma omp single
      tau = 0.0;                                    // compute new coefficient
  
#pragma omp for schedule(static) collapse(2)
      for(int i=0; i<Nx; i++)
        for(int j=0; j<Ny; j++)
	  // using preconditioner M to solve get z ( Mz = r), M is the diagonal matrix of A( Au = f)
	  z[j*Nx+i] = r[j*Nx+i]/(-2.0/hx/hx - 2.0/hy/hy);   

#pragma omp for schedule(static) collapse(2) reduction(+:tau)
      for(int i=0; i<Nx; i++)
        for(int j=0; j<Ny; j++)
	  tau += r[j*Nx+i]*z[j*Nx+i];               // update our tau

      if(k==1){
#pragma omp for schedule(static) 
      for(int kk=0; kk<Nx*Ny; kk++)
	p[kk] = z[kk];                              // set p = z
      }// end if
    else{
#pragma omp single
      beta = tau/tauprev;                           // compute beta

#pragma omp for schedule(static)  
      for(int kk=0; kk<Nx*Ny; kk++)
	p[kk] = z[kk] + beta*p[kk];                 // new search direction                      
    }// end else
      
 #pragma omp for schedule(static) collapse(2)  
    for(int i=1; i<Nx-1; i++)
      for(int j=1; j<Ny-1; j++)
	// auxiliary vector
	w[j*Nx+i] = (-2/hx/hx - 2/hy/hy)*p[j*Nx+i] + p[(j-1)*Nx+i]/hy/hy + p[j*Nx+i-1]/hx/hx + p[j*Nx+i+1]/hx/hx + p[(j+1)*Nx+i]/hy/hy;         

 #pragma omp single 
    dot = 0.0;                                            

 #pragma omp for schedule(static) reduction(+:dot)
    for(int kk=0; kk<Nx*Ny; kk++)
      dot += p[kk]*w[kk];

 #pragma omp single
    {
      alpha = tau/dot;                             // find optimal alpha in line search
      tauprev = tau;                               // save previous coefficient   
      r0_temp = 0.0;
    }

 #pragma omp for schedule(static)
     for(int kk=0; kk<Nx*Ny; kk++){
       u[kk] += alpha*p[kk];                       // new guess
       r[kk] -= alpha*w[kk];                       // recompute residual
     }// end kk also for region

#pragma omp for schedule(static) reduction(+:r0_temp)
     for(int kk=0; kk<Nx*Ny; kk++)
       r0_temp += r[kk]*r[kk];

#pragma omp single
     {
       r0 = r0_temp; 
       k = k + 1;                             // increase number of iterations
      // cout << " the norm of residual = " << sqrt(r0/Nx/Ny) << endl;
     }
    }// end while
}// end parallel region
  cout << " the number of iteration = " << k-1 << endl;
  //cout << " the number of threads: " << nthreads << endl;
  return u;
}

    
    
  
