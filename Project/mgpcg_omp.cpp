/* Lu  Zhang @ SMU
   Math 6370
   Spring 2017     */

// Inclusions
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <omp.h>

using namespace std;

/* this function used to get the solution of linear system Au = f by MGPCG
   the stencil for each point is
                     | 0             1/hy/hy                 0 |
                     | 1/hx/hx    -2/hx/hx - 2/hy/hy   1/hx/hx |
                     | 0             1/hy/hy                 0 |       
   input:
           hx is the cell width in x line
           hy is the cell width in y line
           u is the initial guess of the linear system Au = f
           f is the right hand side of linear system
           eps and kmax determine the stopping criterium
           Nx is the number of grids in x line
           Ny is the number of grids in y line
           nv is the number of calling v cycle
   output:
           u is the solution of the linear system    */

// declaring function prototype
vector<double> vcycle_jcobi_omp(int, int, int, double, double, vector<double>, vector<double>, int, int);

vector<double> mgpcg_omp(double hx, double hy, vector<double> u, vector<double> f, double eps, int kmax, int Nx, int Ny, int nv,int mu1, int mu2){
  
  // declearion
  vector<double> r(Nx*Ny);           
  vector<double> p(Nx*Ny);
  vector<double> w(Nx*Ny);
  vector<double> z(Nx*Ny);
  double tauprev, alpha, normb, tau, beta, dot, r0_temp;
  double r0 = 0.0;                                         // initialize the norm of residual
  int k = 1;                                               // initialize the number of iteration
  // get the nember of levels for multigrid
  int L = min(log10(Nx-1)/log10(2.0), log10(Ny-1)/log10(2.0));
  
#pragma omp parallel default(shared)                       // start the parallel region
  {

#pragma omp for collapse(2) schedule(static)               // initialize r t0 f
    for(int i=0; i<Nx; i++)
      for(int j=0; j<Ny; j++){
	r[j*Nx+i] = f[j*Nx+i];
      }

#pragma omp for collapse(2) schedule(static)
    for(int i=1; i<Nx-1; i++)
      for(int j=1; j<Ny-1; j++)
	// get residual r = f - laplace u
	r[j*Nx+i] += (2.0/hx/hx+2.0/hy/hy)*u[j*Nx+i]-u[(j-1)*Nx+i]/hy/hy-u[j*Nx+i-1]/hx/hx-u[j*Nx+i+1]/hx/hx-u[(j+1)*Nx+i]/hy/hy;
    
#pragma omp for collapse(2) schedule(static) reduction(+:r0)
    for(int i=0; i<Nx; i++)
      for(int j=0; j<Ny; j++)
        r0 += r[j*Nx+i]*r[j*Nx+i];                         // get the norm of our residual
}// end parallel region
   // normb = sqrt(r0)*eps;                                  // precompute the bound of residual size

  // start the loop
  while( sqrt(r0/Nx/Ny) > eps && k < kmax){

    for(int kk=0; kk<Nx*Ny; kk++)
      z[kk] = 0.0;                                        // initialize z
    
    for(int i=0; i<nv; i++)
      z = vcycle_jcobi_omp(L, mu1, mu2, hx, hy, z, r, Nx, Ny);
    
 
    tau = 0.0;                                           // compute new coefficient
      
#pragma omp parallel default(shared)
     {
#pragma omp for collapse(2) schedule(static) reduction(+:tau)
	for(int i=0; i<Nx; i++)
          for(int j=0; j<Ny; j++)
	    tau += r[j*Nx+i]*z[j*Nx+i];                    // compute the value of tau

	if(k==1){
#pragma omp for schedule(static)
	  for(int k=0; k<Nx*Ny; k++)
	    p[k] = z[k];                                   // set p = z
        }
        else{
#pragma omp single
          beta = tau/tauprev;
      
#pragma omp for schedule(static)
          for(int k=0; k<Nx*Ny; k++)
 	    p[k] = z[k] + beta*p[k];                       // new search direction
        }

#pragma omp for collapse(2) schedule(static)
	for(int i=1; i<Nx-1; i++)
          for(int j=1; j<Ny-1; j++)
	    // auxiliary vector
	    w[j*Nx+i] = (-2/hx/hx - 2/hy/hy)*p[j*Nx+i] + p[(j-1)*Nx+i]/hy/hy + p[j*Nx+i-1]/hx/hx + p[j*Nx+i+1]/hx/hx + p[(j+1)*Nx+i]/hy/hy;

#pragma omp single
        dot = 0.0;                                         // initialize dot
	
#pragma omp for schedule(static) reduction(+:dot)
        for(int k=0; k<Nx*Ny; k++)
          dot += p[k]*w[k];                               // compute the value of dot
	
#pragma omp single
	{
          alpha = tau/dot;                                // find optimal alpha in line search
	  tauprev = tau;                                  // save previous coefficient
          r0_temp = 0.0;                                  // initialize r0_temp
	}

#pragma omp for schedule(static)
	for(int k=0; k<Nx*Ny; k++){
          u[k] += alpha*p[k];                             // new guess            
          r[k] -= alpha*w[k];                             // recompute residual
        }
    
#pragma omp for schedule(static) reduction(+:r0_temp)
        for(int k=0; k<Nx*Ny; k++)
          r0_temp += r[k]*r[k];                          // compute norm of new residual
      }//end parallel region
          r0 = r0_temp;
          k = k + 1;     // increase number of iterations
  //cout << " the norm of residual = " << sqrt(r0/Nx/Ny)<<endl;
    }// end while
//  cout << " the norm of residual = " << sqrt(r0/Nx/Ny) << endl;
  cout << " the number of iteration = " << k-1 << endl;
 // cout << " the number of threads: " << nthreads << endl;
  return u;
}

