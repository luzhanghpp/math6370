/* Lu Zhang
   SMU Mathematics
   Math 6370
   19 April 2017 */

// Inclusions
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "mpi.h"

// Prototypes
void chem_solver(double, double*, double*, double*, 
		 double, double, int, int*, double*);


/* Example routine to compute the equilibrium chemical densities at 
   a number of spatial locations, given a (random) background temperature
   field.  The chemical rate equations and solution strategy are in the 
   subroutine chem_solver, which is called at every spatial location. */
int main(int argc, char* argv[]) {

  // declarations
  int maxit, n, i, its, ierr, numprocs, myid, numsent, tag, source;
  double lam, eps, *T, *u, *v, *w, res, runtime;
  double *Pbuf, *Sbuf;
  double stime, ftime;
  bool more_work;
  MPI_Status status;

  // initialize MPI
  ierr = MPI_Init(&argc, &argv);
  if (ierr != MPI_SUCCESS) {
    std::cout << "Error in calling MPI_Init" << std::endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  ierr = MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  if (ierr != MPI_SUCCESS) {
    std::cout << "Error in MPI_Comm_size" << std::endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if(ierr != MPI_SUCCESS) {
    std::cout << "Error in MPI_Comm_rank" << std::endl;
    MPI_Abort(MPI_COMM_WORLD,1);
  }

  if (myid == 0) {
    // 2. input the number of intervals
    std::cout << "Enter the number of intervals (0 quits):" << std::endl;
    std::cin >> n;
    if (n < 1) {
      MPI_Abort(MPI_COMM_WORLD,1);
    }

    // 3. allocate temperature and solution arrays
    T = new double[n];
    u = new double[n];
    v = new double[n];
    w = new double[n];
    Pbuf = new double[4];
    Sbuf = new double[3];

    // 4. set random temperature field, initial guesses at chemical densities
    for (i=0; i<n; i++)  T[i] = random() / (pow(2.0,31.0) - 1.0);
    for (i=0; i<n; i++)  u[i] = 0.35;
    for (i=0; i<n; i++)  v[i] = 0.1;
    for (i=0; i<n; i++)  w[i] = 0.5;

    // 5. start timer
    stime = MPI_Wtime();

    // initialize the number of work that master has sent to
    numsent = 0;

    // send initial tasks to each of the work nodes
    for (i= 0; i<numprocs-1; i++) {
      Pbuf[0] = T[i];
      Pbuf[1] = u[i]; 
      Pbuf[2] = v[i];
      Pbuf[3] = w[i];
      ierr = MPI_Send(Pbuf, 4, MPI_DOUBLE, i+1, i+1, MPI_COMM_WORLD);
      if (ierr != MPI_SUCCESS) {
	std::cout << "Error in MPI_Send" << std::endl;
	MPI_Abort(MPI_COMM_WORLD, 1);
      }
      numsent++;
    }

    // loop over all the intervals(call MPI_Recv to obtain the worker's solution)
    for (i=0; i<n; i++) {
       ierr = MPI_Recv(Sbuf, 3, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      if (ierr != MPI_SUCCESS) {
	std::cout <<  "Error in MPI_Recv" << std::endl;
	MPI_Abort(MPI_COMM_WORLD, 1);
      }

      // master extract the solution form status
      tag = status.MPI_TAG;
      source = status.MPI_SOURCE;
      u[tag-1]=Sbuf[0];
      v[tag-1]=Sbuf[1];
      w[tag-1]=Sbuf[2];

      // send tasks to awaiting workers
      if(numsent < n) {
	Pbuf[0] = T[numsent];
	Pbuf[1] = u[numsent]; 
	Pbuf[2] = v[numsent];
	Pbuf[3] = w[numsent];
	ierr = MPI_Send(Pbuf, 4, MPI_DOUBLE, source, numsent+1 , MPI_COMM_WORLD);
	if (ierr != MPI_SUCCESS) {
	  std::cout <<  "Error in MPI_Send" << std::endl;
	  MPI_Abort(MPI_COMM_WORLD, 1);
	}
	numsent++;
      }
      else{
	tag = 0;
	  ierr = MPI_Send(Pbuf, 4, MPI_DOUBLE, source, tag, MPI_COMM_WORLD);
	  if (ierr != MPI_SUCCESS) {
	    std::cout << "Error in MPI_Send" << std::endl;
	    MPI_Abort(MPI_COMM_WORLD, 1);
	  }
      }
    }

    // 7. stop timer 
    ftime = MPI_Wtime();
    runtime = ftime - stime;

    // 8. output solution time 
    std::cout << "     runtime = " << runtime << std::endl;

    // 9. free temperature and solution arrays 
    delete[] T;
    delete[] u;
    delete[] v;
    delete[] w;
    delete[] Pbuf;
    delete[] Sbuf;

  } else {
    // start worker
    // 1. set solver input parameters
    maxit = 1000000;
    lam = 1.e-2;
    eps = 1.e-10;

    // allocate the local variables for each process
    T = new double[1];
    u = new double[1];
    v = new double[1];
    w = new double[1];
    Pbuf = new double[4];
    Sbuf = new double[3];
 
    // 6. call solver over n intervals
    more_work = true;
    while(more_work) {
      ierr = MPI_Recv(Pbuf, 4, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      if (ierr != MPI_SUCCESS) {
	std::cout << "Error in MPI_Recv" << std::endl;
	MPI_Abort(MPI_COMM_WORLD, 1);
      }

      tag = status.MPI_TAG;
      if (tag == 0){
	more_work = false;
      } else {
	T[0] = Pbuf[0];
	u[0] = Pbuf[1];
	v[0] = Pbuf[2];
	w[0] = Pbuf[3];
	chem_solver(T[0], &u[0], &v[0], &w[0], lam, eps, maxit, &its, &res);
	if (res < eps){ 
	  std::cout << "    i = " << tag-1 <<",  its = " << its << std::endl;
	} else {
	  std::cout << "    error: i= " << tag-1 << ",  its= " << its << ", res=" << res << ", u=" << u[i] << ", v=" << v[i] << ", w=" << w[i] << std::endl;
	  ierr = MPI_Abort(MPI_COMM_WORLD, 1);	
	}

	// send the solution buffer to master
	Sbuf[0] = Pbuf[1];
	Sbuf[1] = Pbuf[2];
	Sbuf[2] = Pbuf[3];
	ierr = MPI_Send(Sbuf, 3, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
	if (ierr != MPI_SUCCESS) {
	  std:: cout << "Error in MPI_Send" << std::endl;
	  MPI_Abort(MPI_COMM_WORLD, 1);
	}
      }
    }

    // deallocate the memory
    delete[] T;
    delete[] u;
    delete[] v;
    delete[] w;
    delete[] Pbuf;
    delete[] Sbuf;
    
  }//end for worker
  
  // finalize MPI 
  ierr = MPI_Finalize();

  return 0;
} // end main 
