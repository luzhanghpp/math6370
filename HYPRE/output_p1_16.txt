 run only with multigrid method:

 hypre_test problem parameters:
   Nx = 16
   Ny = 16
   Px = 1
   Py = 1
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   maxit = 50
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,0.566667]x[-0.5,0.566667],  extents [0,15]x[0,15]

 hypre_test: solving matrix system 
   lin resid = 4.4e-06 (tol = 1.0e-03), its = 3

 hypre_test runtimes:
   initialization time = 0.00452995
   overall solve time  = 0.00214219
   PFMG solve time     = 0.00133681
   overall run time    = 0.0066731

 run with multigrid as preconditioner:

 hypre_test problem parameters:
   Nx = 16
   Ny = 16
   Px = 1
   Py = 1
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   PCGmaxit = 50
   PFMGmaxit = 1
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,0.566667]x[-0.5,0.566667],  extents [0,15]x[0,15]

 hypre_test: solving matrix system 
   lin resid = 3.5e-07 (tol = 1.0e-03), PCG its = 3, PFMG its = 1

 hypre_test runtimes:
   initialization time = 0.013047
   overall solve time  = 0.010612
   PCG solve time      = 0.00226116
   overall run time    = 0.0236599

