 run only with multigrid method:

 hypre_test problem parameters:
   Nx = 4096
   Ny = 4096
   Px = 1
   Py = 1
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   maxit = 50
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,0.500244]x[-0.5,0.500244],  extents [0,4095]x[0,4095]

 hypre_test: solving matrix system 
   lin resid = 3.2e-04 (tol = 1.0e-03), its = 6

 hypre_test runtimes:
   initialization time = 1.50808
   overall solve time  = 34.3778
   PFMG solve time     = 12.1905
   overall run time    = 35.8859

 run with multigrid as preconditioner:

 hypre_test problem parameters:
   Nx = 4096
   Ny = 4096
   Px = 1
   Py = 1
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   PCGmaxit = 50
   PFMGmaxit = 1
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,0.500244]x[-0.5,0.500244],  extents [0,4095]x[0,4095]

 hypre_test: solving matrix system 
   lin resid = 7.9e-06 (tol = 1.0e-03), PCG its = 4, PFMG its = 1

 hypre_test runtimes:
   initialization time = 1.53476
   overall solve time  = 37.8469
   PCG solve time      = 12.7318
   overall run time    = 39.3816

