 run only with multigrid method:
 p9 [coords = (1,1)]:  subdomain [-0.37451,-0.24902]x[-0.37451,-0.24902],  extents [32,63]x[32,63]
 p17 [coords = (2,1)]:  subdomain [-0.24902,-0.123529]x[-0.37451,-0.24902],  extents [64,95]x[32,63]
 p33 [coords = (4,1)]:  subdomain [0.00196078,0.127451]x[-0.37451,-0.24902],  extents [128,159]x[32,63]
 p18 [coords = (2,2)]:  subdomain [-0.24902,-0.123529]x[-0.24902,-0.123529],  extents [64,95]x[64,95]
 p10 [coords = (1,2)]:  subdomain [-0.37451,-0.24902]x[-0.24902,-0.123529],  extents [32,63]x[64,95]
 p12 [coords = (1,4)]:  subdomain [-0.37451,-0.24902]x[0.00196078,0.127451],  extents [32,63]x[128,159]
 p25 [coords = (3,1)]:  subdomain [-0.123529,0.00196078]x[-0.37451,-0.24902],  extents [96,127]x[32,63]
 p8 [coords = (1,0)]:  subdomain [-0.37451,-0.24902]x[-0.5,-0.37451],  extents [32,63]x[0,31]
 p11 [coords = (1,3)]:  subdomain [-0.37451,-0.24902]x[-0.123529,0.00196078],  extents [32,63]x[96,127]
 p20 [coords = (2,4)]:  subdomain [-0.24902,-0.123529]x[0.00196078,0.127451],  extents [64,95]x[128,159]
 p26 [coords = (3,2)]:  subdomain [-0.123529,0.00196078]x[-0.24902,-0.123529],  extents [96,127]x[64,95]
 p27 [coords = (3,3)]:  subdomain [-0.123529,0.00196078]x[-0.123529,0.00196078],  extents [96,127]x[96,127]
 p13 [coords = (1,5)]:  subdomain [-0.37451,-0.24902]x[0.127451,0.252941],  extents [32,63]x[160,191]
 p14 [coords = (1,6)]:  subdomain [-0.37451,-0.24902]x[0.252941,0.378431],  extents [32,63]x[192,223]
 p19 [coords = (2,3)]:  subdomain [-0.24902,-0.123529]x[-0.123529,0.00196078],  extents [64,95]x[96,127]
 p57 [coords = (7,1)]:  subdomain [0.378431,0.503922]x[-0.37451,-0.24902],  extents [224,255]x[32,63]
 p21 [coords = (2,5)]:  subdomain [-0.24902,-0.123529]x[0.127451,0.252941],  extents [64,95]x[160,191]
 p16 [coords = (2,0)]:  subdomain [-0.24902,-0.123529]x[-0.5,-0.37451],  extents [64,95]x[0,31]
 p22 [coords = (2,6)]:  subdomain [-0.24902,-0.123529]x[0.252941,0.378431],  extents [64,95]x[192,223]
 p28 [coords = (3,4)]:  subdomain [-0.123529,0.00196078]x[0.00196078,0.127451],  extents [96,127]x[128,159]
 p35 [coords = (4,3)]:  subdomain [0.00196078,0.127451]x[-0.123529,0.00196078],  extents [128,159]x[96,127]
 p15 [coords = (1,7)]:  subdomain [-0.37451,-0.24902]x[0.378431,0.503922],  extents [32,63]x[224,255]
 p34 [coords = (4,2)]:  subdomain [0.00196078,0.127451]x[-0.24902,-0.123529],  extents [128,159]x[64,95]
 p59 [coords = (7,3)]:  subdomain [0.378431,0.503922]x[-0.123529,0.00196078],  extents [224,255]x[96,127]
 p29 [coords = (3,5)]:  subdomain [-0.123529,0.00196078]x[0.127451,0.252941],  extents [96,127]x[160,191]
 p58 [coords = (7,2)]:  subdomain [0.378431,0.503922]x[-0.24902,-0.123529],  extents [224,255]x[64,95]
 p30 [coords = (3,6)]:  subdomain [-0.123529,0.00196078]x[0.252941,0.378431],  extents [96,127]x[192,223]
 p36 [coords = (4,4)]:  subdomain [0.00196078,0.127451]x[0.00196078,0.127451],  extents [128,159]x[128,159]
 p23 [coords = (2,7)]:  subdomain [-0.24902,-0.123529]x[0.378431,0.503922],  extents [64,95]x[224,255]
 p31 [coords = (3,7)]:  subdomain [-0.123529,0.00196078]x[0.378431,0.503922],  extents [96,127]x[224,255]
 p37 [coords = (4,5)]:  subdomain [0.00196078,0.127451]x[0.127451,0.252941],  extents [128,159]x[160,191]
 p39 [coords = (4,7)]:  subdomain [0.00196078,0.127451]x[0.378431,0.503922],  extents [128,159]x[224,255]
 p32 [coords = (4,0)]:  subdomain [0.00196078,0.127451]x[-0.5,-0.37451],  extents [128,159]x[0,31]
 p60 [coords = (7,4)]:  subdomain [0.378431,0.503922]x[0.00196078,0.127451],  extents [224,255]x[128,159]
 p61 [coords = (7,5)]:  subdomain [0.378431,0.503922]x[0.127451,0.252941],  extents [224,255]x[160,191]
 p41 [coords = (5,1)]:  subdomain [0.127451,0.252941]x[-0.37451,-0.24902],  extents [160,191]x[32,63]
 p38 [coords = (4,6)]:  subdomain [0.00196078,0.127451]x[0.252941,0.378431],  extents [128,159]x[192,223]
 p42 [coords = (5,2)]:  subdomain [0.127451,0.252941]x[-0.24902,-0.123529],  extents [160,191]x[64,95]
 p49 [coords = (6,1)]:  subdomain [0.252941,0.378431]x[-0.37451,-0.24902],  extents [192,223]x[32,63]
 p62 [coords = (7,6)]:  subdomain [0.378431,0.503922]x[0.252941,0.378431],  extents [224,255]x[192,223]
 p56 [coords = (7,0)]:  subdomain [0.378431,0.503922]x[-0.5,-0.37451],  extents [224,255]x[0,31]
 p24 [coords = (3,0)]:  subdomain [-0.123529,0.00196078]x[-0.5,-0.37451],  extents [96,127]x[0,31]
 p1 [coords = (0,1)]:  subdomain [-0.5,-0.37451]x[-0.37451,-0.24902],  extents [0,31]x[32,63]
 p44 [coords = (5,4)]:  subdomain [0.127451,0.252941]x[0.00196078,0.127451],  extents [160,191]x[128,159]
 p43 [coords = (5,3)]:  subdomain [0.127451,0.252941]x[-0.123529,0.00196078],  extents [160,191]x[96,127]
 p50 [coords = (6,2)]:  subdomain [0.252941,0.378431]x[-0.24902,-0.123529],  extents [192,223]x[64,95]
 p63 [coords = (7,7)]:  subdomain [0.378431,0.503922]x[0.378431,0.503922],  extents [224,255]x[224,255]
 p45 [coords = (5,5)]:  subdomain [0.127451,0.252941]x[0.127451,0.252941],  extents [160,191]x[160,191]
 p51 [coords = (6,3)]:  subdomain [0.252941,0.378431]x[-0.123529,0.00196078],  extents [192,223]x[96,127]
 p40 [coords = (5,0)]:  subdomain [0.127451,0.252941]x[-0.5,-0.37451],  extents [160,191]x[0,31]
 p46 [coords = (5,6)]:  subdomain [0.127451,0.252941]x[0.252941,0.378431],  extents [160,191]x[192,223]
 p52 [coords = (6,4)]:  subdomain [0.252941,0.378431]x[0.00196078,0.127451],  extents [192,223]x[128,159]
 p54 [coords = (6,6)]:  subdomain [0.252941,0.378431]x[0.252941,0.378431],  extents [192,223]x[192,223]
 p53 [coords = (6,5)]:  subdomain [0.252941,0.378431]x[0.127451,0.252941],  extents [192,223]x[160,191]
 p55 [coords = (6,7)]:  subdomain [0.252941,0.378431]x[0.378431,0.503922],  extents [192,223]x[224,255]
 p47 [coords = (5,7)]:  subdomain [0.127451,0.252941]x[0.378431,0.503922],  extents [160,191]x[224,255]
 p48 [coords = (6,0)]:  subdomain [0.252941,0.378431]x[-0.5,-0.37451],  extents [192,223]x[0,31]
 p2 [coords = (0,2)]:  subdomain [-0.5,-0.37451]x[-0.24902,-0.123529],  extents [0,31]x[64,95]
 p3 [coords = (0,3)]:  subdomain [-0.5,-0.37451]x[-0.123529,0.00196078],  extents [0,31]x[96,127]
 p4 [coords = (0,4)]:  subdomain [-0.5,-0.37451]x[0.00196078,0.127451],  extents [0,31]x[128,159]
 p5 [coords = (0,5)]:  subdomain [-0.5,-0.37451]x[0.127451,0.252941],  extents [0,31]x[160,191]
 p6 [coords = (0,6)]:  subdomain [-0.5,-0.37451]x[0.252941,0.378431],  extents [0,31]x[192,223]
 p7 [coords = (0,7)]:  subdomain [-0.5,-0.37451]x[0.378431,0.503922],  extents [0,31]x[224,255]

 hypre_test problem parameters:
   Nx = 256
   Ny = 256
   Px = 8
   Py = 8
   numprocs = 64
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   maxit = 50
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,-0.37451]x[-0.5,-0.37451],  extents [0,31]x[0,31]

 hypre_test: solving matrix system 
   lin resid = 1.1e-04 (tol = 1.0e-03), its = 4

 hypre_test runtimes:
   initialization time = 0.0308971
   overall solve time  = 0.182297
   PFMG solve time     = 0.0687549
   overall run time    = 0.213224

 run with multigrid as preconditioner:
 p9 [coords = (1,1)]:  subdomain [-0.37451,-0.24902]x[-0.37451,-0.24902],  extents [32,63]x[32,63]
 p17 [coords = (2,1)]:  subdomain [-0.24902,-0.123529]x[-0.37451,-0.24902],  extents [64,95]x[32,63]
 p33 [coords = (4,1)]:  subdomain [0.00196078,0.127451]x[-0.37451,-0.24902],  extents [128,159]x[32,63]
 p10 [coords = (1,2)]:  subdomain [-0.37451,-0.24902]x[-0.24902,-0.123529],  extents [32,63]x[64,95]
 p19 [coords = (2,3)]:  subdomain [-0.24902,-0.123529]x[-0.123529,0.00196078],  extents [64,95]x[96,127]
 p25 [coords = (3,1)]:  subdomain [-0.123529,0.00196078]x[-0.37451,-0.24902],  extents [96,127]x[32,63]
 p34 [coords = (4,2)]:  subdomain [0.00196078,0.127451]x[-0.24902,-0.123529],  extents [128,159]x[64,95]
 p18 [coords = (2,2)]:  subdomain [-0.24902,-0.123529]x[-0.24902,-0.123529],  extents [64,95]x[64,95]
 p11 [coords = (1,3)]:  subdomain [-0.37451,-0.24902]x[-0.123529,0.00196078],  extents [32,63]x[96,127]
 p49 [coords = (6,1)]:  subdomain [0.252941,0.378431]x[-0.37451,-0.24902],  extents [192,223]x[32,63]
 p26 [coords = (3,2)]:  subdomain [-0.123529,0.00196078]x[-0.24902,-0.123529],  extents [96,127]x[64,95]
 p27 [coords = (3,3)]:  subdomain [-0.123529,0.00196078]x[-0.123529,0.00196078],  extents [96,127]x[96,127]
 p20 [coords = (2,4)]:  subdomain [-0.24902,-0.123529]x[0.00196078,0.127451],  extents [64,95]x[128,159]
 p21 [coords = (2,5)]:  subdomain [-0.24902,-0.123529]x[0.127451,0.252941],  extents [64,95]x[160,191]
 p35 [coords = (4,3)]:  subdomain [0.00196078,0.127451]x[-0.123529,0.00196078],  extents [128,159]x[96,127]
 p12 [coords = (1,4)]:  subdomain [-0.37451,-0.24902]x[0.00196078,0.127451],  extents [32,63]x[128,159]
 p36 [coords = (4,4)]:  subdomain [0.00196078,0.127451]x[0.00196078,0.127451],  extents [128,159]x[128,159]
 p28 [coords = (3,4)]:  subdomain [-0.123529,0.00196078]x[0.00196078,0.127451],  extents [96,127]x[128,159]
 p13 [coords = (1,5)]:  subdomain [-0.37451,-0.24902]x[0.127451,0.252941],  extents [32,63]x[160,191]
 p22 [coords = (2,6)]:  subdomain [-0.24902,-0.123529]x[0.252941,0.378431],  extents [64,95]x[192,223]
 p8 [coords = (1,0)]:  subdomain [-0.37451,-0.24902]x[-0.5,-0.37451],  extents [32,63]x[0,31]
 p16 [coords = (2,0)]:  subdomain [-0.24902,-0.123529]x[-0.5,-0.37451],  extents [64,95]x[0,31]
 p50 [coords = (6,2)]:  subdomain [0.252941,0.378431]x[-0.24902,-0.123529],  extents [192,223]x[64,95]
 p30 [coords = (3,6)]:  subdomain [-0.123529,0.00196078]x[0.252941,0.378431],  extents [96,127]x[192,223]
 p38 [coords = (4,6)]:  subdomain [0.00196078,0.127451]x[0.252941,0.378431],  extents [128,159]x[192,223]
 p24 [coords = (3,0)]:  subdomain [-0.123529,0.00196078]x[-0.5,-0.37451],  extents [96,127]x[0,31]
 p39 [coords = (4,7)]:  subdomain [0.00196078,0.127451]x[0.378431,0.503922],  extents [128,159]x[224,255]
 p41 [coords = (5,1)]:  subdomain [0.127451,0.252941]x[-0.37451,-0.24902],  extents [160,191]x[32,63]
 p14 [coords = (1,6)]:  subdomain [-0.37451,-0.24902]x[0.252941,0.378431],  extents [32,63]x[192,223]
 p42 [coords = (5,2)]:  subdomain [0.127451,0.252941]x[-0.24902,-0.123529],  extents [160,191]x[64,95]
 p57 [coords = (7,1)]:  subdomain [0.378431,0.503922]x[-0.37451,-0.24902],  extents [224,255]x[32,63]
 p52 [coords = (6,4)]:  subdomain [0.252941,0.378431]x[0.00196078,0.127451],  extents [192,223]x[128,159]
 p23 [coords = (2,7)]:  subdomain [-0.24902,-0.123529]x[0.378431,0.503922],  extents [64,95]x[224,255]
 p53 [coords = (6,5)]:  subdomain [0.252941,0.378431]x[0.127451,0.252941],  extents [192,223]x[160,191]
 p31 [coords = (3,7)]:  subdomain [-0.123529,0.00196078]x[0.378431,0.503922],  extents [96,127]x[224,255]
 p32 [coords = (4,0)]:  subdomain [0.00196078,0.127451]x[-0.5,-0.37451],  extents [128,159]x[0,31]
 p1 [coords = (0,1)]:  subdomain [-0.5,-0.37451]x[-0.37451,-0.24902],  extents [0,31]x[32,63]
 p59 [coords = (7,3)]:  subdomain [0.378431,0.503922]x[-0.123529,0.00196078],  extents [224,255]x[96,127]
 p15 [coords = (1,7)]:  subdomain [-0.37451,-0.24902]x[0.378431,0.503922],  extents [32,63]x[224,255]
 p44 [coords = (5,4)]:  subdomain [0.127451,0.252941]x[0.00196078,0.127451],  extents [160,191]x[128,159]
 p29 [coords = (3,5)]:  subdomain [-0.123529,0.00196078]x[0.127451,0.252941],  extents [96,127]x[160,191]
 p37 [coords = (4,5)]:  subdomain [0.00196078,0.127451]x[0.127451,0.252941],  extents [128,159]x[160,191]
 p51 [coords = (6,3)]:  subdomain [0.252941,0.378431]x[-0.123529,0.00196078],  extents [192,223]x[96,127]
 p43 [coords = (5,3)]:  subdomain [0.127451,0.252941]x[-0.123529,0.00196078],  extents [160,191]x[96,127]
 p45 [coords = (5,5)]:  subdomain [0.127451,0.252941]x[0.127451,0.252941],  extents [160,191]x[160,191]
 p54 [coords = (6,6)]:  subdomain [0.252941,0.378431]x[0.252941,0.378431],  extents [192,223]x[192,223]
 p55 [coords = (6,7)]:  subdomain [0.252941,0.378431]x[0.378431,0.503922],  extents [192,223]x[224,255]
 p58 [coords = (7,2)]:  subdomain [0.378431,0.503922]x[-0.24902,-0.123529],  extents [224,255]x[64,95]
 p46 [coords = (5,6)]:  subdomain [0.127451,0.252941]x[0.252941,0.378431],  extents [160,191]x[192,223]
 p60 [coords = (7,4)]:  subdomain [0.378431,0.503922]x[0.00196078,0.127451],  extents [224,255]x[128,159]
 p61 [coords = (7,5)]:  subdomain [0.378431,0.503922]x[0.127451,0.252941],  extents [224,255]x[160,191]
 p40 [coords = (5,0)]:  subdomain [0.127451,0.252941]x[-0.5,-0.37451],  extents [160,191]x[0,31]
 p62 [coords = (7,6)]:  subdomain [0.378431,0.503922]x[0.252941,0.378431],  extents [224,255]x[192,223]
 p56 [coords = (7,0)]:  subdomain [0.378431,0.503922]x[-0.5,-0.37451],  extents [224,255]x[0,31]
 p48 [coords = (6,0)]:  subdomain [0.252941,0.378431]x[-0.5,-0.37451],  extents [192,223]x[0,31]
 p47 [coords = (5,7)]:  subdomain [0.127451,0.252941]x[0.378431,0.503922],  extents [160,191]x[224,255]
 p63 [coords = (7,7)]:  subdomain [0.378431,0.503922]x[0.378431,0.503922],  extents [224,255]x[224,255]
 p2 [coords = (0,2)]:  subdomain [-0.5,-0.37451]x[-0.24902,-0.123529],  extents [0,31]x[64,95]
 p3 [coords = (0,3)]:  subdomain [-0.5,-0.37451]x[-0.123529,0.00196078],  extents [0,31]x[96,127]
 p4 [coords = (0,4)]:  subdomain [-0.5,-0.37451]x[0.00196078,0.127451],  extents [0,31]x[128,159]
 p5 [coords = (0,5)]:  subdomain [-0.5,-0.37451]x[0.127451,0.252941],  extents [0,31]x[160,191]
 p6 [coords = (0,6)]:  subdomain [-0.5,-0.37451]x[0.252941,0.378431],  extents [0,31]x[192,223]
 p7 [coords = (0,7)]:  subdomain [-0.5,-0.37451]x[0.378431,0.503922],  extents [0,31]x[224,255]

 hypre_test problem parameters:
   Nx = 256
   Ny = 256
   Px = 8
   Py = 8
   numprocs = 64
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   PCGmaxit = 50
   PFMGmaxit = 1
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,-0.37451]x[-0.5,-0.37451],  extents [0,31]x[0,31]

 hypre_test: solving matrix system 
   lin resid = 2.5e-05 (tol = 1.0e-03), PCG its = 3, PFMG its = 1

 hypre_test runtimes:
   initialization time = 0.0298872
   overall solve time  = 0.197724
   PCG solve time      = 0.0823791
   overall run time    = 0.227612

