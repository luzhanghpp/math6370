 run only with multigrid method:

 hypre_test problem parameters:
   Nx = 32
   Ny = 32
   Px = 1
   Py = 1
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   maxit = 50
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,0.532258]x[-0.5,0.532258],  extents [0,31]x[0,31]

 hypre_test: solving matrix system 
   lin resid = 1.2e-05 (tol = 1.0e-03), its = 3

 hypre_test runtimes:
   initialization time = 0.00426102
   overall solve time  = 0.00287294
   PFMG solve time     = 0.00183201
   overall run time    = 0.00713491

 run with multigrid as preconditioner:

 hypre_test problem parameters:
   Nx = 32
   Ny = 32
   Px = 1
   Py = 1
   mu_x = 1
   mu_y = 1
   xL = -0.5
   xR = 0.5
   yL = -0.5
   yR = 0.5
   tol = 0.001
   PCGmaxit = 50
   PFMGmaxit = 1
   relch = 1
   rlxtype = 3
   npre = 3
   npost = 3

 p0 [coords = (0,0)]:  subdomain [-0.5,0.532258]x[-0.5,0.532258],  extents [0,31]x[0,31]

 hypre_test: solving matrix system 
   lin resid = 6.0e-07 (tol = 1.0e-03), PCG its = 3, PFMG its = 1

 hypre_test runtimes:
   initialization time = 0.00363803
   overall solve time  = 0.00425291
   PCG solve time      = 0.00317597
   overall run time    = 0.00789213

