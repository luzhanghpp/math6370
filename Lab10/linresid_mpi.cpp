/* Lu Zhang
   SMU Mathematics
   Math 4370/6370
   12 April 2017 */

// Inclusions
#include <stdlib.h>
#include <math.h>
#include <mpi.h>


// Description: calculates the linear residual and its averaged 2-norm (WRMS)
int linresid(double *a, double *b, double *c, double *u, 
	     double *r, double *res, double &norm2, int N, MPI_Comm comm) {

  // local variables
  int ierr=0, nprocs, my_id, k;
  double norm;
  double sl, sr, ul, ur;
  MPI_Status status;

  // get MPI parallelism information from comm
  ierr = MPI_Comm_size(comm, &nprocs);
  if (ierr != MPI_SUCCESS){
    fprintf(stderr, " error in MPI_Comm_size = %i\n", ierr);
    MPI_Abort(comm, 1);
  }
  ierr = MPI_Comm_rank(comm, &my_id);
  if (ierr != MPI_SUCCESS) {
    fprintf(stderr, " error in MPI_Comm_rank = %i\n", ierr);
    MPI_Abort(comm, 1);
  }

  // initialize send/recv buffers
  sl = u[0];
  sr = u[N-1];
  ul = 0.0;
  ur = 0.0;

  // phase 1: even my_id send right, odd my_id rec left  
  if(my_id%2 == 0){
    if(my_id < nprocs-1){
      ierr = MPI_Send(&sr, 1, MPI_DOUBLE, my_id+1, 1, comm);
      if (ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Send = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  } else {
    if(my_id > 0){
      ierr = MPI_Recv(&ul, 1, MPI_DOUBLE, my_id-1, 1, comm, &status);
      if(ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Recv = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  }

  // phase 2: even my_id rec right, odd my_id send left
  if(my_id%2 == 0){
    if(my_id < nprocs-1){
      ierr = MPI_Recv(&ur, 1, MPI_DOUBLE, my_id+1, 2, comm, &status);
      if (ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Send = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  } else {
    if(my_id > 0){
      ierr = MPI_Send(&sl, 1, MPI_DOUBLE, my_id-1, 2, comm);
      if(ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Recv = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  }

  // phase 3: odd my_id send right, even my_id rec left
  if(my_id%2 == 1){
    if(my_id < nprocs-1){
      ierr = MPI_Send(&sr, 1, MPI_DOUBLE, my_id+1, 3, comm);
      if (ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Send = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  } else {
    if(my_id > 0){
      ierr = MPI_Recv(&ul, 1, MPI_DOUBLE, my_id-1, 3, comm, &status);
      if(ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Recv = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  }
 
  // phase 4: odd my_id rec right, even my_id send left
  if(my_id%2 == 1){
    if(my_id < nprocs-1){
      ierr = MPI_Recv(&ur, 1, MPI_DOUBLE, my_id+1, 4, comm, &status);
      if (ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Send = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  } else {
    if(my_id > 0){
      ierr = MPI_Send(&sl, 1, MPI_DOUBLE, my_id-1, 4, comm);
      if(ierr != MPI_SUCCESS){
        fprintf(stderr, " error in MPI_Recv = %i\n", ierr);
        MPI_Abort(comm, 1);
      }
    }
  }  
   
  // compute linear residual in the interior of subdomain
  norm = 0.0;
  for(k = 1; k<N-2; k++){
    res[k] = a[k]*u[k-1]+b[k]*u[k]+c[k]*u[k+1]-r[k];
    norm += res[k]*res[k];
  }

  // compute linear residual at left edge of local subdomain
  k=0;
  res[k] = a[k]*ul+b[k]*u[k]+c[k]*u[k+1]-r[k];
  norm += res[k]*res[k];

  // compute linear residual at right edge of local subdomain
  k = N-1;
  res[k] = a[k]*u[k-1] + b[k]*u[k] + c[k]*ur - r[k];
  norm += res[k]*res[k];
  
  // combine local sums into global L2-norm
  ierr = MPI_Allreduce(&norm, &norm2, 1, MPI_DOUBLE, MPI_SUM, comm);
  if(ierr !=MPI_SUCCESS){
    printf(" error in MPI_Allreduce = %i\n", ierr);
    MPI_Abort(comm, 1);
  }

  norm2 = sqrt(norm2)/N/nprocs;

  
  return ierr;
} // end linresid
