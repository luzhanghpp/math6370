iterative test with 1 processors
    gamma = 0.1
    linear solver tolerance delta = 1e-10
    global problem size N = 10000000
    local problem sizes n = 10000000
 initial residual: ||T*u-r||_2 = 0.000316228
 converged in 9 iterations at delta = 1e-10
 solution time: 1.06659 seconds
 final residual: ||T*u-r||_2 = 3.1379e-11
