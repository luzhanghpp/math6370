-O0 versus -O2
the total time for the -O0 is 7.10489e+00 seconds and for -O2 is 3.882537e+00 seconds, it shows that the compiler which uses -02 makes the code about 45% faster than the original one.

original versus hand-optimized
the total time for the original is 7.10489e+00 seconds and for hand-optimized is 3.089688e+00 seconds, it shows that the hand-optimized is about 56.5% faster than the original one.

Finally, the hand-optimized code which allows the compiler to optimize spends about 9.01787-01 seconds. So we can conclude that the hand-optimized code with complier optimized can be very fast. It is almost about 87% faster than the original one.
